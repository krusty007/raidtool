<?php
session_start();
include("db_lib.php");

if(isset($_REQUEST['pw_edit_mode'])){
    $array_ref     = $db->check_login();
    $ergebnis      = $array_ref['ergebnis'];
    $token         = $array_ref['token'];
    $gruppenplaner = $array_ref['gruppenplaner'];
    $raidplan      = $array_ref['raidplan'];
    $vorlage       = $array_ref['vorlage'];
    $gilde         = $array_ref['gilde'];
    $gildenstatus  = $array_ref['gildenstatus'];
    $user_id       = $array_ref['user_id'];
    $fraktion      = $array_ref['fraktion'];
    $sprache       = $array_ref['sprache'];
    if($ergebnis){
        $_SESSION['token']         = $token;
        $_SESSION['gruppenplaner'] = $gruppenplaner;
        $_SESSION['raidplan']      = $raidplan;
        $_SESSION['vorlage']       = $vorlage;
        $_SESSION['gilde']         = $gilde;
        $_SESSION['gildenstatus']  = $gildenstatus;
        $_SESSION['user_id']       = $user_id;
        $_SESSION['fraktion']      = $fraktion;
        $_SESSION['sprache']       = $sprache;

        $url = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $url = $db->remove_url_param( $url, 'activate' );
        $escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );

        $url_add = '';
        if(!isset($_GET["token"])){
            $url_add = "?token=".$_SESSION['token'];
        }

        header("Location: ".$escaped_url.$url_add);
        die();
    }
}

if(isset($_REQUEST['logout'])){
    // unset($_SESSION['sprache']);
    session_unset();

    // nach dem Logout auf die Startseite springen

    $url =  "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
    $escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );

    $suchmuster = '/\?.*/';
    $escaped_url = preg_replace($suchmuster, "", $escaped_url);

    header("Location: ".$escaped_url);
    die();
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
<head>
    <title>Raidtool</title>
    <link href="default.css" type="text/css" rel="stylesheet" />
<?php
    if($db->editmode()){
        print '<link href="default_edit.css" type="text/css" rel="stylesheet" />';
    }
?>
    <script type="text/javascript" src="jquery-1.11.1.js"></script>
    <script type="text/javascript" src="jquery-ui.js"></script>
    <script type="text/javascript" src="default.js"></script>
    <link rel="icon" type="image/vnd.microsoft.icon" href="favicon.ico">
    <?php
        $show_raid = 0;
        if(isset($_REQUEST['token'])){
            if($db->check_token($_REQUEST['token']) == 1){
                $show_raid = 1;
            }
        }

        if(isset($_REQUEST['token']) and $show_raid){
            $fraktion = $db->get_fraktion_via_token($_REQUEST['token']);
            print '<style>';
            if($fraktion == "1"){
                print 'body{background-image: url("wallpaper_horde.jpg");}';
            }
            else{
                print 'body{background-image: url("wallpaper_allianz.jpg");}';
            }
            print '</style>';
        }
    ?>
</head>
<body>
    <script type="text/javascript" src="wz_tooltip.js"></script>
<?php

$ausgabe = '';

if(isset($_REQUEST['activate'])){
    $aktiviert = $db->get_user_aktiviert($_REQUEST['activate']);

    $ausgabe .= '<div class="activate_info">';
    if($aktiviert){
        $ausgabe .= $db->get_db_text("!!Aktivierung_ok!!");
    }
    else{
        $ausgabe .= $db->get_db_text("!!Aktivierung_nicht_ok!!");
    }
    $ausgabe .= '</div>';
}

$ausgabe .= $db->loginfenster();

if($show_raid){
    list($gildenname,$realm,$fraktion) = $db->get_gildendaten_via_token($_REQUEST['token']);
    $bg_color = '#D94743';
    $color    = '#232227';
    if($fraktion == 2){
        $bg_color = '#1a3685';
        $color    = '#fce900';
    }

    $text_raid           = $db->get_db_text("!!Raid!!");
    $text_boss           = $db->get_db_text("!!Boss!!");
    $text_token          = $db->get_db_text("!!zur_Tokeneingabe!!");
    $text_char_in_liste  = $db->get_db_text("!!Charakter_in_Liste!!");
    $text_char_verteilt  = $db->get_db_text("!!Charakter_verteilt!!");
    $text_char_gesamt    = $db->get_db_text("!!Charakter_gesamt!!");
    $text_gruppenplanung = $db->get_db_text("!!Gruppenplanung!!");
    $text_anzeigen       = $db->get_db_text("!!Anzeigen!!");
    $text_raidplan       = $db->get_db_text("!!Raidplan!!");
    $text_verwaltung     = $db->get_db_text("!!Verwaltung!!");

    $ausgabe .= '<div class="auswahlfenster">'.$db->get_addon_auswahl().$text_raid.': '.$db->get_schlachtzug().'<br><br>
    '.$text_boss.': <select id="boss_liste" onchange="select_raidaufstellung();"><option></option></select> <button onclick="select_raidaufstellung();" id="btn_refersh" style="display:none;"><img src="refresh.png"></button>
        <div style="float:right;margin-left:10px;">
            <img src="up.png" id="prev_boss" style="display:none;"><br><img src="down.png" id="next_boss" style="display:none;">
        </div>
    </div>

    <div class="auswahlfenster home_div" id="fenster_home" style="margin-left:50px;text-align:center;" onclick="goto(\'home\');">
        '.$text_token.'
    </div>

    <div class="auswahlfenster" id="fenster_statistik" style="margin-left:50px;display:none;">
        <table>
            <tr>
                <td style="text-align:center;width:200px;font-weight:bold;">'.$text_char_in_liste.'</td>
                <td style="text-align:center;width:200px;font-weight:bold;">'.$text_char_verteilt.'</td>
                <td style="text-align:center;width:200px;font-weight:bold;">'.$text_char_gesamt.'</td>
            </tr>
            <tr>
                <td style="text-align:center;" id="anzahl_liste">0</td>
                <td style="text-align:center;" id="anzahl_bereiche">0</td>
                <td style="text-align:center;" id="anzahl_gesamt">0</td>
            </tr>
            <tr>
                <td colspan="3" style="text-align:center;background-color:'.$bg_color.';font-weight:bold;color:'.$color.'">{ '.$realm.' } - '.$gildenname.'</td>
            </tr>
        </table>
    </div>';

    if($db->editmode()){
        if($_SESSION['gruppenplaner'] == 1){
            $ausgabe .= '
                <div class="auswahlfenster" id="fenster_gruppenplanung" style="margin-left:50px;text-align:center;display:none;">
                    '.$text_gruppenplanung.'<br>
                    <button onclick="open_gruppe();" id="gruppe_button">'.$text_anzeigen.'</button>
                </div>
            ';
        }

        $ausgabe .= '
            <div class="taktik_main" style="display:none;">
                <div class="taktik_box" style="display:none;">'.$db->get_taktik_symbole().'</div>
                <div class="switcher" onclick="toggle_taktik();"><</div>
            </div>
        ';
    }

    if($db->get_app_via_name('raidplan')){
        $ausgabe .= '
            <div class="auswahlfenster" id="fenster_raidplan" style="margin-left:50px;text-align:center;padding:6px;">
                <button id="button_raidplan" onclick="open_raidplan();" style="background-color:#AEDEE7;padding:10px;font-weight:bold;font-size:18px;">'.$text_raidplan.'</button>
            </div>
        ';
    }

    if($db->editmode()){
        if($_SESSION['gildenstatus'] == 2){
            $ausgabe .= '
                <div class="auswahlfenster" id="fenster_member" style="margin-left:50px;text-align:center;padding:6px;">
                    <button id="button_raidplan" onclick="open_memberwindow();" class="button_verwaltung">'.$text_verwaltung.'</button>
                </div>
            ';
        }
    }

    $ausgabe .= '<div style="clear:both;"></div>';

    $ausgabe .= '<br><div id="userliste" class="userliste"></div>
    <div id="main" class="main"></div>
    <div style="float:left;width:206px;">';

    if($db->editmode()){
        $text_charakter_enternen = $db->get_db_text("!!Charakter_entfernen!!");
        $text_neuer_bereich = $db->get_db_text("!!neuer_Bereich!!");

        $ausgabe .= '
            <div id="systemuserliste" class="userliste" style="min-height:22px;height:auto;display:none;"></div>
            <div id="delete_area" class="userliste delete_area" style="display:none;">'.$text_charakter_enternen.'</div>
            <div style="clear:both;"></div>
            <div style="float:left;margin-top:30px;">
                <button onclick="add_yard();" class="r_button" style="display:none;padding:5px;width:100%;" id="add_area">'.$text_neuer_bereich.'</button>
            </div>';

        if($_SESSION['vorlage'] == 1){
            $ausgabe .= '
                <div style="float:left;margin-top:30px;height:auto;display:none;" class="userliste" id="vorlage_div">
                    <select style="width:100%;font-size:16px;" id="vorlage_vorhanden" onchange="set_vorlage();"></select><br><br><br>
                    Neue Vorlage unter:<br>
                    <input type="text" style="width:192px;" id="vorlage_name" placeholder="Bezeichnung"><br>
                    <button style="width:200px;background-color: #B2E39B;" onclick="save_vorlage();">Speichern</button><br><br><br>
                    <select style="width:100%;font-size:16px;" id="vorlage_delete"></select><br>
                    <button style="width:200px;background-color: #D46A6A;color:#fff;" onclick="delete_vorlage();">Löschen</button>
                </div>
            ';
        }
    }

    $ausgabe .= '
        <div style="clear:both;"></div>
        <div class="info_div" style="display:none;margin-top:24px;">
            <div class="info_tank" onclick="get_class_info(1);"></div>
            <div class="info_heal" onclick="get_class_info(2);"></div>
            <div class="info_damage" onclick="get_class_info(3);"></div>
        </div>
        <div style="clear:both;"></div>
        <div class="meldung_div" style="display:none;margin-top:100px;">
            <div class="info_meldung" onclick="open_img_meldung();"></div>
        </div>
        </div>';

    if($db->editmode()){
        $text_eigenes_bild               = $db->get_db_text("!!eigenes_bild!!");
        $text_import                     = $db->get_db_text("!!Import!!");
        $text_charakterliste_leeren      = $db->get_db_text("!!Charakterliste_leeren!!");
        $text_charakter_hinzu            = $db->get_db_text("!!Charakter_hinzu!!");
        $text_charakterliste_importieren = $db->get_db_text("!!Charakterliste_importieren!!");
        $text_aendern                    = $db->get_db_text("!!Aendern!!");

        $ausgabe .= '
        <div style="clear:both;"></div>
        <div id="div_import" style="display:none;">
            <input type="text" id="boss_bild_url" style="margin-top:0px;margin-left:226px;width:950px;" placeholder="'.$text_eigenes_bild.'" /> <button onclick="save_own_boss_img();">'.$text_aendern.'</button><br>
            <textarea id="import_text" rows="3" cols="176" style="margin-top:5px;" placeholder="'.$text_import.'"></textarea><br>
            <div class="auswahlfenster">
                <label style="cursor:pointer;"><input type="radio" id="import_option_1" name="import_option"> '.$text_charakterliste_leeren.'</label><br>
                <label style="cursor:pointer;"><input type="radio" id="import_option_2" name="import_option" checked="checked"> '.$text_charakter_hinzu.'</label><br><br>
                <button onclick="user_import();">'.$text_charakterliste_importieren.'</button>
            </div>
        </div>';
    }
}
else{
    $text_auswaehlen = $db->get_db_text("!!Auswaehlen!!");

    $ausgabe .= '
        <div class="auswahlfenster" style="margin-left:auto;margin-right:auto;text-align:center;padding:6px;width:350px;float:none;"><form onsubmit="open_token(); return false;">
            Token:
            <input type="text" maxlength="1" id="token_input1" onkeyup="enter_token(1,event);" onfocusin="fokusin_token(this);" onfocusout="fokusout_token();" autocomplete="off" class="token_input" >
            <input type="text" maxlength="1" id="token_input2" onkeyup="enter_token(2,event);" onfocusin="fokusin_token(this);" onfocusout="fokusout_token();" autocomplete="off" class="token_input" >
            <input type="text" maxlength="1" id="token_input3" onkeyup="enter_token(3,event);" onfocusin="fokusin_token(this);" onfocusout="fokusout_token();" autocomplete="off" class="token_input" >
            <input type="text" maxlength="1" id="token_input4" onkeyup="enter_token(4,event);" onfocusin="fokusin_token(this);" onfocusout="fokusout_token();" autocomplete="off" class="token_input" >
            <input type="text" maxlength="1" id="token_input5" onkeyup="enter_token(5,event);" onfocusin="fokusin_token(this);" onfocusout="fokusout_token();" autocomplete="off" class="token_input" >
            <input type="submit" id="token_button" disabled="disabled" value="'.$text_auswaehlen.'"></form>
        </div>
    ';

    $ausgabe .= '<div class="auswahlfenster" style="margin-left:auto;margin-right:auto;margin-top:20px;text-align:center;padding:6px;width:350px;float:none;">';
    $ausgabe .= $db->get_language(1);
    $ausgabe .= '</div>';

}

$ausgabe .= '<div style="clear:both;"></div>';

$ausgabe .= $db->register_window();

$ausgabe .= '<div class="faq_button" onclick="open_faq();"></div>';

print $ausgabe;
?>

</body>
</html>
