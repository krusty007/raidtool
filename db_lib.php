<?php
class datenbank{
    function get_db_zugangsdaten($val_parameter=""){
        $filename = "zugangsdaten.txt";
        if (!file_exists($filename)) {
            fopen($filename, "w");

            $current = file_get_contents($filename);
            $current .= "name=\"\";\nhost=\"localhost\";\nuser=\"root\";\npass=\"\";";

            file_put_contents($filename, $current);

            print '<br><span style="color:#f00;font-weight:bold;font-family:Lucida Console;">Datei für den Datenbankzugang existiert nicht und wurde angelegt.<br>Bitte fehlende Felder ergänzen!</span><br>';
        }

        $lines = file($filename);

        $access_array = array();

        foreach ($lines as $line_num => $line) {
            preg_replace("/\\n/", "", $line);

            if(substr($line, 0, 2) == "//" or substr($line, 0, 1) == ";")continue;

            $line = trim($line);

            preg_match('/(.*)="(.*)";/', $line, $match, PREG_OFFSET_CAPTURE);

            $key = (isset($match[1][0]))?$match[1][0]:'';

            if($key){
                $wert = (isset($match[2][0]))?$match[2][0]:'';
                $access_array[$key] = $wert;
            }
        }

        return $access_array;
    }

    function sql_prepare($val_parameter=""){
        $db_zugang = $this->get_db_zugangsdaten();
        $dbname = $db_zugang['name'];
        $dbhost = $db_zugang['host'];
        $dbuser = $db_zugang['user'];
        $dbpass = $db_zugang['pass'];

        if(!$dbname or !$dbhost or !$dbuser){
            print '<br><span style="color:#f00;font-weight:bold;font-family:Lucida Console;">Zugangsdaten für Datenbank nicht vorhanden!</span><br>';
        }

        $dbh = new PDO('mysql:host='.$dbhost.';dbname='.$dbname, $dbuser, $dbpass);
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $arr_return = array();

        $statement = '';
        $execute = '';

        $sql_statement = (is_array($val_parameter[1]))?$val_parameter[0]:$val_parameter;
        $statement = $dbh->prepare($sql_statement);

        try{
            if(is_array($val_parameter[1])){
                $execute = $statement->execute($val_parameter[1]);
            }
            else{
                $execute = $statement->execute();
            }
        } catch(PDOException $e){
            die($this->screen($e->getMessage()));
        }

        return array($statement,$execute);
    }

    function select ($val_parameter=""){
        $array_sql = $this->sql_prepare($val_parameter);

        $statement = $array_sql[0];
        $execute = $array_sql[1];

        $arr_return = array();
        if($execute) {
            while($row = $statement->fetch()) {
                $arr_return[] = $row;
            }
        }

        return $arr_return;
    }

    function query ($val_parameter=""){
        $array_sql = $this->sql_prepare($val_parameter);

        $statement = $array_sql[0];
        $execute = $array_sql[1];

        $row_count = $statement->rowCount();

        return $row_count;
    }

    function screen ($val_parameter=""){
        if(empty($val_parameter)){
            print '<pre style="background-color:#f8b2b2;border:2px solid #b60000;text-align:center;padding-left:10px;color:#000000;"><br />Die Variable ist leer!<br /><br /></pre>';
        }else{
            print '<pre style="background-color:#bde5fc;border:2px solid #44a1d6;text-align:left;padding-left:10px;color:#000000;">';
            print_r( $val_parameter );
            print '</pre>';
        }
    }

    function get_schlachtzug($val_parameter=""){
        $ausgabe = '<select id="schlachtzug" onchange="load_boss_liste();"><option></option>';

        $sprache = $this->get_user_language();

        $statement = "
            SELECT
                schlachtzug.id,
                schlachtzug.bezeichnung,
                schlachtzug.FARBE,
                schlachtzug.ADDON,
                (
                    SELECT
                        ".$sprache."
                    FROM
                        raideinteilung_lingua
                    WHERE
                        ID = schlachtzug.LINGUA_ID
                ) AS ANZEIGE
            FROM
                schlachtzug
                INNER JOIN raideinteilung_gilden AS gilde ON gilde.ADDON = schlachtzug.ADDON
            WHERE
                schlachtzug.SPERRE = 0
            AND
                gilde.ID = ?
            ORDER BY
                schlachtzug.sortierung ASC
        ";
        $schlachtzug_inhalt = $this->select(array($statement,array($this->get_gilde_id_via_token($_REQUEST['token']))));

        foreach($schlachtzug_inhalt as $key2 => $var){
            $style = '';
            if($var['FARBE']){
                $style = 'style="background-color:'.$var['FARBE'].';"';

            }

            $anzeige = ($var['ANZEIGE'])?$var['ANZEIGE']:$var['bezeichnung'];
            if($_SERVER['HTTP_HOST'] != "localhost"){
                $anzeige = utf8_encode($anzeige);
            }

            $ausgabe .= '<option value="'.$var['id'].'" '.$style.' data-addon="'.$var['ADDON'].'">'.$anzeige.'</option>';
        }

        $ausgabe .= '</select>';

        return $ausgabe;
    }

    function get_bossliste($val_parameter=""){
        $sprache = $this->get_user_language();

        $statement = "
            SELECT
                id,
                bezeichnung,
                (
                    SELECT
                        ".$sprache."
                    FROM
                        raideinteilung_lingua
                    WHERE
                        ID = bosse.LINGUA_ID
                ) AS ANZEIGE
            FROM
                bosse
            WHERE
                schlachtzug_id = ?
            AND
                SPERRE = 0
            ORDER BY
                SORTIERUNG,
                id
        ";

        $boss_liste = $this->select(array($statement,array($val_parameter)));

        $inhalt = '<option></option>';
        foreach($boss_liste as $key => $var){
            $id   = $var['id'];
            $name = utf8_decode($var['bezeichnung']);

            $anzeige = ($var['ANZEIGE'])?$var['ANZEIGE']:$name;

            $inhalt .= '<option value="'.$id.'">'.$anzeige.'</option>';
        }

        $inhalt = utf8_encode($inhalt);

        return $inhalt;
    }

    function clear_user_aktuell($val_parameter=''){
        $statement = "
            SELECT
                GROUP_CONCAT(raideinteilung_user.ID) AS USERLISTE
            FROM
                raideinteilung_user
            WHERE
                raideinteilung_user.SYSTEMUSER = 0
        ";

        $user_liste = $this->select($statement);

        $statement = "
            DELETE FROM
                raideinteilung_user_aktuell
            WHERE
                ID IN ( ".$user_liste[0]['USERLISTE']." )
        ";

        $this->query($statement);

        $statement = "
            SELECT
                GROUP_CONCAT(bosse.id) AS BOSSLISTE
            FROM
                schlachtzug
                INNER JOIN bosse ON bosse.schlachtzug_id = schlachtzug.id
            WHERE
                schlachtzug.id = ?
        ";

        $boss_liste = $this->select(array($statement,array($val_parameter)));

        $statement = "
            DELETE FROM raideinteilung_boss_user_platz_zuordnung
            WHERE
                raideinteilung_boss_user_platz_zuordnung.BOSS_ID IN ( ".$boss_liste[0]['BOSSLISTE']." )
        ";

        $this->query($statement);
    }

    function set_char_aktuell($val_parameter=''){
        if(!$val_parameter){
            return;
        }

        $user_default = utf8_decode($val_parameter);

        $user_id = $this->get_user_id_via_name($user_default);

        if($user_id){
            $statement = "
                SELECT
                    ID
                FROM
                    raideinteilung_user_aktuell
                WHERE
                    ID = ?
            ";

            $aktuell_vorhanden_statement = $this->select(array($statement,array($user_id)));

            if(!$aktuell_vorhanden_statement){
                // User ID in die aktuelle Tabelle eintragen, sollte sie nicht vorhanden sein
                $statement = "
                    INSERT INTO raideinteilung_user_aktuell
                    (
                        ID
                    )
                    VALUES
                    (
                        ?
                    )
                ";

                $this->query(array($statement,array($user_id)));
            }
        }
    }

    function get_user_id_via_name($val_parameter){
        $user_default = $val_parameter;

        // schauen, ob der User in der Tabelle überhaupt existiert
        $statement = "
            SELECT
                ID
            FROM
                raideinteilung_user
            WHERE
                USERNAME = ?
            AND
                GILDE = ?
        ";

        $db_user_id = $this->select(array($statement,array($user_default,$_SESSION['gilde'])));

        $user_id = '';
        if($db_user_id){
            $user_id = $db_user_id[0]['ID'];
        }

        if(!$user_id){
            $rasse  = 9;
            $klasse = 1;

            // char in datenbank hinzufügen
            $statement = "
                INSERT INTO raideinteilung_user
                (
                    USERNAME,
                    RASSE,
                    KLASSE,
                    GILDE
                )
                VALUES
                (
                    ?,
                    ?,
                    ?,
                    ?
                )
            ";

            $this->query(array($statement,array($user_default,$rasse,$klasse,$_SESSION['gilde'])));

            $statement = "
                SELECT
                    ID
                FROM
                    raideinteilung_user
                ORDER BY
                    ID DESC
                LIMIT 1
            ";

            $user_id_statement = $this->select($statement);

            $user_id = $user_id_statement[0]['ID'];
        }

        return $user_id;
    }

    function add_area($value=""){
        $boss_id = $value[0];
        $typ     = $value[1];

        $titel = 'TITEL';
        if($typ == 'TAKTIK'){
            $titel = $_POST['symbol_id'];
        }

        $statement = "
            INSERT INTO raideinteilung_plaetze
            (
                P_LEFT,
                P_TOP,
                P_TITEL,
                BOSS_ID,
                TYP,
                GILDE
            )
            VALUES
            (
                0,
                0,
                ?,
                ?,
                ?,
                ?
            )
        ";

        $this->query(array($statement,array($titel,$boss_id,$typ,$_SESSION['gilde'])));

        $this->set_boss_change($boss_id);

        $statement = "
            SELECT
                ID
            FROM
                raideinteilung_plaetze
            WHERE
                GILDE = ?
            ORDER BY
                ID DESC
            LIMIT 1
        ";

        $area_id = $this->select(array($statement,array($_SESSION['gilde'])));

        return $area_id[0]['ID'];
    }

    function save_area_position($val_parameter=""){
        $id   = $val_parameter[0];
        $top  = $val_parameter[1];
        $left = $val_parameter[2];

        $statement = "
            UPDATE raideinteilung_plaetze SET
                P_LEFT = ?,
                P_TOP = ?
            WHERE
                ID = ?
        ";

        $this->query(array($statement,array($left,$top,$id)));

        $statement = "
            SELECT
                BOSS_ID
            FROM
                raideinteilung_plaetze
            WHERE
                ID = ?
        ";

        $boss_id = $this->select(array($statement,array($id)));

        $this->set_boss_change($boss_id[0]['BOSS_ID']);

        return;
    }

    function load_area($boss_id=""){
        $raid_id = $this->get_raid_id_via_boss_id($boss_id);

        $planungsmodus = $this->get_planungsmodus($raid_id);

        $statement = "
            SELECT
                ID,
                P_LEFT,
                P_TOP,
                P_TITEL,
                TYP
            FROM
                raideinteilung_plaetze
            WHERE
                BOSS_ID = ?
            AND
                GILDE = ?
        ";

        $area_liste = $this->select(array($statement,array($boss_id,$this->get_gilde_id_via_token($_REQUEST['token']))));
        $ausgabe = '';
        foreach($area_liste as $key => $var){
            $id    = $var['ID'];
            $left  = $var['P_LEFT'];
            $top   = $var['P_TOP'];
            $typ   = $var['TYP'];
            $titel = utf8_decode($var['P_TITEL']);

            $edit = '';
            $delete_taktik = '';
            if($this->editmode()){
                $edit = ' <img src="edit.png" id="btn_edit_'.$id.'" style="cursor:pointer;" onclick="edit_area(\''.$id.'\');"> <img src="delete.png" style="cursor:pointer;" onclick="delete_area(\''.$id.'\')">';
                $delete_taktik = 'ondblclick="delete_taktik(\''.$id.'\');"';
            }

            $area_inhalt = '';
            if(!$this->editmode() and $planungsmodus == 1){
                $area_inhalt = '';
            }else{
                $area_inhalt = $this->get_box_inhalt($id);
            }

            $default_titel = $titel;

            $titel = $this->replace_symbole($titel);

            if($typ == 'BOX'){
                $ausgabe .= '<div class="area" id="area_'.$id.'" style="left:'.$left.'px;top:'.$top.'px;z-index:2;"><div class="area_title" data-default="'.$default_titel.'"><span>'.$titel.'</span>'.$edit.'</div>'.$area_inhalt.'</div>';
            }
            elseif($typ == 'TAKTIK'){
                $bild_name = $this->get_taktik_symbol_url($titel);
                $height = '';
                switch($titel){
                    case '15': $height = 'height:50px;';break;
                    case '16': $height = 'height:50px;';break;
                    case '17': $height = 'height:50px;';break;
                    case '18': $height = 'height:50px;';break;
                }
                $ausgabe .= '<img src="'.$bild_name.'" id="area_'.$id.'" '.$delete_taktik.' class="taktik_area" style="left:'.$left.'px;top:'.$top.'px;z-index:1;'.$height.'">';
            }
        }

        $statement = "
            SELECT
                BILD
            FROM
                bosse
            WHERE
                id = ?
        ";

        $bild = $this->select(array($statement,array($boss_id)));

        $bild_ausgabe = $bild[0]['BILD'];

        if(isset($_REQUEST['token'])){
            $gilde_id = $this->get_gilde_id_via_token($_REQUEST['token']);

            $statement = "
                SELECT
                    BILD_URL
                FROM
                    bosse_bilder
                WHERE
                    GILDE_ID = ?
                AND
                    BOSS_ID = ?
            ";

            $bild_neu = $this->select(array($statement,array($gilde_id,$boss_id)));

            if($bild_neu){
                $bild_ausgabe = $bild_neu[0]['BILD_URL'];
            }
        }

        $tank_inhalt = '';
        $heal_inhalt = '';
        $damage_inhalt = '';
        if($this->get_class_info(array($boss_id,1,1))){$tank_inhalt = '1';}
        if($this->get_class_info(array($boss_id,2,1))){$heal_inhalt = '1';}
        if($this->get_class_info(array($boss_id,3,1))){$damage_inhalt = '1';}

        return array($ausgabe,$bild_ausgabe,$tank_inhalt,$heal_inhalt,$damage_inhalt);
    }

    function delete_area($val_parameter=""){
        $area_id = $val_parameter[0];
        $boss_id = $val_parameter[1];

        $statement = "DELETE FROM raideinteilung_vorlage WHERE BOSS_ID = ? AND PLATZ_ID = ?";

        $this->query(array($statement,array($boss_id,$area_id)));

        $statement = "DELETE FROM raideinteilung_boss_user_platz_zuordnung WHERE BOSS_ID = ? AND PLATZ_ID = ?";

        $this->query(array($statement,array($boss_id,$area_id)));

        $statement = "DELETE FROM raideinteilung_plaetze WHERE ID = ?";

        $this->query(array($statement,array($area_id)));

        $this->set_boss_change($boss_id);

        return;
    }

    function edit_text_area($val_parameter=""){
        $area_id = $val_parameter[0];
        $inhalt  = $val_parameter[1];

        $inhalt = $this->utf8_to_html($inhalt);

        $statement = "
            UPDATE raideinteilung_plaetze SET
                P_TITEL = ?
            WHERE
                ID = ?
        ";

        $this->query(array($statement,array($inhalt,$area_id)));

        $statement = "
            SELECT
                P_TITEL
            FROM
                raideinteilung_plaetze
            WHERE
                ID = ?
        ";

        $area_inhalt = $this->select(array($statement,array($area_id)));

        $textinhalt = $area_inhalt[0]['P_TITEL'];
        $textinhalt_default = $textinhalt;

        $textinhalt = $this->replace_symbole($textinhalt);

        $statement = "
            SELECT
                BOSS_ID
            FROM
                raideinteilung_plaetze
            WHERE
                ID = ?
        ";

        $boss_id = $this->select(array($statement,array($area_id)));

        $this->set_boss_change($boss_id[0]['BOSS_ID']);

        return array($textinhalt,$textinhalt_default);
    }

    function replace_symbole($val_parameter=""){
        $val_parameter = preg_replace("/{TOTENKOPF}/i", '<img src="symbole/totenkopf.png">', $val_parameter);
        $val_parameter = preg_replace("/{KREUZ}/i", '<img src="symbole/kreuz.png">', $val_parameter);
        $val_parameter = preg_replace("/{QUADRAT}/i", '<img src="symbole/quadrat.png">', $val_parameter);
        $val_parameter = preg_replace("/{MOND}/i", '<img src="symbole/mond.png">', $val_parameter);
        $val_parameter = preg_replace("/{DREIECK}/i", '<img src="symbole/dreieck.png">', $val_parameter);
        $val_parameter = preg_replace("/{DIAMANT}/i", '<img src="symbole/diamant.png">', $val_parameter);
        $val_parameter = preg_replace("/{KREIS}/i", '<img src="symbole/kreis.png">', $val_parameter);
        $val_parameter = preg_replace("/{STERN}/i", '<img src="symbole/stern.png">', $val_parameter);
        $val_parameter = preg_replace("/&&/i", '<br>', $val_parameter);

        return $val_parameter;
    }

    function drop_user($val_parameter=""){
        $boss_id = $val_parameter[2];
        $user_id = $val_parameter[1];
        $area_id = $val_parameter[0];

        $this->reset_user(array($user_id,$boss_id));

        $sort_id = $this->get_last_sort_id_via_platz_id($area_id);

        $statement = "
            INSERT INTO raideinteilung_boss_user_platz_zuordnung
            (
                BOSS_ID,
                USER_ID,
                PLATZ_ID,
                SORTIERUNG
            )
            VALUES
            (
                ?,
                ?,
                ?,
                ?
            )
        ";

        $this->query(array($statement,array($boss_id,$user_id,$area_id,$sort_id)));

        $inhalt = $this->get_box_inhalt($area_id);

        $this->set_boss_change($boss_id);

        return $inhalt;
    }

    function get_last_sort_id_via_platz_id($val_parameter=""){
        $area_id = $val_parameter;

        $statement = "
            SELECT
                IFNULL(MAX(SORTIERUNG),0)+1 AS SORTIERUNG
            FROM
                raideinteilung_boss_user_platz_zuordnung
            WHERE
                PLATZ_ID = ?
        ";

        $sort_statement = $this->select(array($statement,array($area_id)));

        return $sort_statement[0]['SORTIERUNG'];
    }

    function get_box_inhalt($val_parameter=""){
        $area_id = $val_parameter;

        $statement = "
            SELECT
                USER_ID,
                raideinteilung_user.USERNAME,
                raideinteilung_klasse.FARBE AS KLASSE_FARBE,
                SORTIERUNG,
                (
                    SELECT
                        MIN(SORTIERUNG)
                    FROM
                        raideinteilung_boss_user_platz_zuordnung
                    WHERE
                        PLATZ_ID = ?
                ) AS MIN_SORTIERUNG,
                (
                    SELECT
                        MAX(SORTIERUNG)
                    FROM
                        raideinteilung_boss_user_platz_zuordnung
                    WHERE
                        PLATZ_ID = ?
                ) AS MAX_SORTIERUNG,
                raideinteilung_user.SYSTEMUSER
            FROM
                raideinteilung_boss_user_platz_zuordnung
                INNER JOIN raideinteilung_user ON raideinteilung_boss_user_platz_zuordnung.USER_ID = raideinteilung_user.ID
                LEFT JOIN raideinteilung_klasse ON raideinteilung_klasse.ID = raideinteilung_user.KLASSE
            WHERE
                raideinteilung_boss_user_platz_zuordnung.PLATZ_ID = ?
            ORDER BY
                raideinteilung_boss_user_platz_zuordnung.SORTIERUNG ASC,
                raideinteilung_user.KLASSE ASC,
                raideinteilung_user.USERNAME ASC
        ";

        $array_inhalt = $this->select(array($statement,array($area_id,$area_id,$area_id)));

        $inhalt = '';
        foreach($array_inhalt as $key => $var){
            $user_id        = $var['USER_ID'];
            $username       = utf8_encode($var['USERNAME']);
            $klasse_farbe   = $var['KLASSE_FARBE'];
            $sortierung     = $var['SORTIERUNG'];
            $min_sortierung = $var['MIN_SORTIERUNG'];
            $max_sortierung = $var['MAX_SORTIERUNG'];
            $systemuser     = $var['SYSTEMUSER'];

            $style = 'background-color:#D8D8D8;';
            if($klasse_farbe){
                $style = 'background-color:'.$klasse_farbe.';';
            }

            $sort_inhalt = '';
            if($this->editmode()){
                if($sortierung < $max_sortierung){
                    $sort_inhalt .= '<div onclick="sort_down(\''.$user_id.'\');" class="sort_down"></div> ';
                }else{
                    $sort_inhalt .= '<img src="clear.png" style="margin-bottom:-3px;float:left;margin-right:3px;"></span> ';
                }

                if($sortierung > $min_sortierung){
                    $sort_inhalt .= '<div onclick="sort_up(\''.$user_id.'\');" class="sort_up"></div> ';
                }else{
                    $sort_inhalt .= '<img src="clear.png" style="margin-bottom:-3px;float:left;margin-right:3px;"></span> ';
                }
            }

            $inhalt .= '<div class="user_box" id="box_item_'.$user_id.'" style="color:#000;'.$style.'" data-system="'.$systemuser.'">'.$sort_inhalt.$username.'</div>';
        }

        return $inhalt;
    }

    function load_userliste($val_parameter=""){
        $boss_id = $val_parameter;

        $raid_id = $this->get_raid_id_via_boss_id($boss_id);

        $planungsmodus = $this->get_planungsmodus($raid_id);

        if(!$this->editmode() and $planungsmodus == 1){
            return '';
        }

        $gilde = 0;
        if(isset($_REQUEST['token'])){
            $gilde = $this->get_gilde_id_via_token($_REQUEST['token']);
        }

        $statement = "
            SELECT
                raideinteilung_user_aktuell.ID,
                raideinteilung_user.USERNAME,
                raideinteilung_klasse.FARBE AS KLASSE_FARBE
            FROM
                raideinteilung_user_aktuell
                INNER JOIN raideinteilung_user ON raideinteilung_user_aktuell.ID = raideinteilung_user.ID
                LEFT JOIN raideinteilung_klasse ON raideinteilung_klasse.ID = raideinteilung_user.KLASSE
            WHERE
                raideinteilung_user.SYSTEMUSER = 0
            AND
                raideinteilung_user.GILDE = ?
            ORDER BY
                raideinteilung_user.KLASSE ASC,
                raideinteilung_user.USERNAME ASC
        ";

        $userliste = $this->select(array($statement,array($gilde)));

        $statement = "
            SELECT
                USER_ID
            FROM
                raideinteilung_boss_user_platz_zuordnung
            WHERE
                BOSS_ID = ?
        ";

        $userliste2 = $this->select(array($statement,array($boss_id)));

        $array_user = array();
        foreach ($userliste2 as $key => $var) {
            array_push($array_user, $var['USER_ID']);
        }

        $ausgabe = '';
        foreach ($userliste as $key => $var) {
            $id           = $var['ID'];
            $username     = utf8_encode($var['USERNAME']);
            $klasse_farbe = $var['KLASSE_FARBE'];

            if(in_array($id, $array_user)){
                continue;
            }

            $style = 'background-color:#D8D8D8;';
            if($klasse_farbe){
                $style = 'background-color:'.$klasse_farbe.';';
            }

            $ondblclick = '';
            if($this->editmode()){
                $ondblclick = 'ondblclick="open_char_edit('.$id.');"';
            }

            $ausgabe .= '<div class="user_box" id="box_item_'.$id.'" style="color:#000;'.$style.'" '.$ondblclick.'>'.$username.'</div>';
        }

        return $ausgabe;
    }

    function load_systemuserliste($val_parameter=""){
        $boss_id = $val_parameter;

        $raid_id = $this->get_raid_id_via_boss_id($boss_id);

        if(!$this->editmode()){
            return '';
        }

        $gilde = 0;
        if(isset($_REQUEST['token'])){
            $gilde = $this->get_gilde_id_via_token($_REQUEST['token']);
        }

        $statement = "
            SELECT
                raideinteilung_user.ID,
                raideinteilung_user.USERNAME
            FROM
                raideinteilung_user
            WHERE
                raideinteilung_user.SYSTEMUSER = 1
            AND
                raideinteilung_user.GILDE = ?
            ORDER BY
                raideinteilung_user.ID ASC
        ";

        $userliste = $this->select(array($statement,array($gilde)));

        $statement = "
            SELECT
                USER_ID
            FROM
                raideinteilung_boss_user_platz_zuordnung
            WHERE
                BOSS_ID = ?
        ";

        $userliste2 = $this->select(array($statement,array($boss_id)));

        $array_user = array();
        foreach ($userliste2 as $key => $var) {
            array_push($array_user, $var['USER_ID']);
        }

        $ausgabe = '';
        foreach ($userliste as $key => $var) {
            $id           = $var['ID'];
            $username     = utf8_encode($var['USERNAME']);

            if(in_array($id, $array_user)){
                continue;
            }

            $ausgabe .= '<div class="user_box" id="box_item_'.$id.'" style="color:#000;background-color:#D8D8D8;">'.$username.'</div>';
        }

        return $ausgabe;
    }

    function reset_user($val_parameter=""){
        $user_id = $val_parameter[0];
        $boss_id = $val_parameter[1];

        $statement = "
            DELETE FROM raideinteilung_boss_user_platz_zuordnung
            WHERE
                BOSS_ID = ?
            AND
                USER_ID = ?
        ";

        $this->query(array($statement,array($boss_id,$user_id)));

        $this->set_boss_change($boss_id);

        return;
    }

    function get_raid_id_via_boss_id($val_parameter=""){
        $boss_id = $val_parameter;

        $statement = "
            SELECT
                schlachtzug_id AS RAID_ID
            FROM
                bosse
            WHERE
                id = ?
        ";

        $raid_id = $this->select(array($statement,array($boss_id)));

        return $raid_id[0]['RAID_ID'];
    }

    function clear_userliste($val_parameter=""){
        $statement = "DELETE FROM raideinteilung_boss_user_platz_zuordnung";

        $this->query($statement);

        $statement = "
            SELECT
                GROUP_CONCAT(raideinteilung_user.ID) AS USERLISTE
            FROM
                raideinteilung_user
            WHERE
                raideinteilung_user.SYSTEMUSER = 0
        ";

        $user_liste = $this->select($statement);

        $statement = "
            DELETE FROM
                raideinteilung_user_aktuell
            WHERE
                ID IN ( ".$user_liste[0]['USERLISTE']." )
        ";

        $this->query($statement);

        return;
    }

    function sort_area($val_parameter=""){
        $boss_id  = $val_parameter[0];
        $area_id  = $val_parameter[1];
        $user_id  = $val_parameter[2];
        $richtung = $val_parameter[3];

        if($richtung == 'down'){
            $statement = "
                SELECT
                    SORTIERUNG
                FROM
                    raideinteilung_boss_user_platz_zuordnung
                WHERE
                    BOSS_ID = ?
                AND
                    USER_ID = ?
                AND
                    PLATZ_ID = ?
            ";

            $sortier_stm = $this->select(array($statement,array($boss_id,$user_id,$area_id)));

            $sort_temp = $sortier_stm[0]['SORTIERUNG'];

            $statement = "
                SELECT
                    SORTIERUNG,
                    USER_ID
                FROM
                    raideinteilung_boss_user_platz_zuordnung
                WHERE
                    BOSS_ID = ?
                AND
                    USER_ID <> ?
                AND
                    PLATZ_ID = ?
                AND
                    SORTIERUNG > ?
                ORDER BY
                    SORTIERUNG ASC
                LIMIT 1
            ";

            $sortier_next_stm = $this->select(array($statement,array($boss_id,$user_id,$area_id,$sort_temp)));

            if(!$sortier_next_stm){
                $inhalt = $this->get_box_inhalt($area_id);

                return $inhalt;
            }

            $sort_temp2 = $sortier_next_stm[0]['SORTIERUNG'];
            $user_temp2 = $sortier_next_stm[0]['USER_ID'];

            $statement = "
                UPDATE raideinteilung_boss_user_platz_zuordnung
                SET
                    SORTIERUNG = ?
                WHERE
                    BOSS_ID = ?
                AND
                    USER_ID = ?
                AND
                    PLATZ_ID = ?
            ";

            $this->query(array($statement,array($sort_temp2,$boss_id,$user_id,$area_id)));

            $statement = "
                UPDATE raideinteilung_boss_user_platz_zuordnung
                SET
                    SORTIERUNG = ?
                WHERE
                    BOSS_ID = ?
                AND
                    USER_ID = ?
                AND
                    PLATZ_ID = ?
            ";

            $this->query(array($statement,array($sort_temp,$boss_id,$user_temp2,$area_id)));
        }

        if($richtung == 'up'){
            $statement = "
                SELECT
                    SORTIERUNG
                FROM
                    raideinteilung_boss_user_platz_zuordnung
                WHERE
                    BOSS_ID = ?
                AND
                    USER_ID = ?
                AND
                    PLATZ_ID = ?
            ";

            $sortier_stm = $this->select(array($statement,array($boss_id,$user_id,$area_id)));

            $sort_temp = $sortier_stm[0]['SORTIERUNG'];

            $statement = "
                SELECT
                    SORTIERUNG,
                    USER_ID
                FROM
                    raideinteilung_boss_user_platz_zuordnung
                WHERE
                    BOSS_ID = ?
                AND
                    USER_ID <> ?
                AND
                    PLATZ_ID = ?
                AND
                    SORTIERUNG < ?
                ORDER BY
                    SORTIERUNG DESC
                LIMIT 1
            ";

            $sortier_next_stm = $this->select(array($statement,array($boss_id,$user_id,$area_id,$sort_temp)));

            if(!$sortier_next_stm){
                $inhalt = $this->get_box_inhalt($area_id);

                return $inhalt;
            }

            $sort_temp2 = $sortier_next_stm[0]['SORTIERUNG'];
            $user_temp2 = $sortier_next_stm[0]['USER_ID'];

            $statement = "
                UPDATE raideinteilung_boss_user_platz_zuordnung
                SET
                    SORTIERUNG = ?
                WHERE
                    BOSS_ID = ?
                AND
                    USER_ID = ?
                AND
                    PLATZ_ID = ?
            ";

            $this->query(array($statement,array($sort_temp2,$boss_id,$user_id,$area_id)));

            $statement = "
                UPDATE raideinteilung_boss_user_platz_zuordnung
                SET
                    SORTIERUNG = ?
                WHERE
                    BOSS_ID = ?
                AND
                    USER_ID = ?
                AND
                    PLATZ_ID = ?
            ";

            $this->query(array($statement,array($sort_temp,$boss_id,$user_temp2,$area_id)));
        }

        $inhalt = $this->get_box_inhalt($area_id);

        return $inhalt;
    }

    function get_planungsmodus($val_parameter=""){
        $statement = "
            SELECT
                PLANUNG
            FROM
                schlachtzug
            WHERE
                id = ?
        ";

        $planung = $this->select(array($statement,array($val_parameter)));

        $planungsmodus = 0;
        if($planung){
            $planungsmodus = $planung[0]['PLANUNG'];
        }

        return $planungsmodus;
    }

    function set_planungsmodus($val_parameter=""){
        $raid_id = $val_parameter[0];
        $wert    = $val_parameter[1];

        $planung = ($wert == "an")?1:0;

        $statement = "
            UPDATE schlachtzug
            SET
                PLANUNG = ?
            WHERE
                id = ?
        ";

        $planung = $this->query(array($statement,array($planung,$raid_id)));

        return;
    }

    function utf8_to_html($text=""){
        $text = str_replace("ä", "&auml;", $text);
        $text = str_replace("ö", "&ouml;", $text);
        $text = str_replace("ü", "&uuml;", $text);
        $text = str_replace("Ä", "&Auml;", $text);
        $text = str_replace("Ö", "&Ouml;", $text);
        $text = str_replace("Ü", "&Uuml;", $text);
        $text = str_replace("ß", "&szlig;", $text);

        return $text;
    }

    function delete_user($val_parameter=""){
        $user_id = $val_parameter[0];
        $boss_id = $val_parameter[1];

        $statement = "
            SELECT
                SYSTEMUSER
            FROM
                raideinteilung_user
            WHERE
                ID = ?
        ";

        $systemuser = $this->select(array($statement,array($user_id)));

        if($systemuser[0]['SYSTEMUSER'] == '1'){
            return;
        }

        $statement = "
            DELETE FROM raideinteilung_boss_user_platz_zuordnung
            WHERE
                USER_ID = ?
        ";

        $this->query(array($statement,array($user_id)));

        $statement = "
            DELETE FROM raideinteilung_user_aktuell
            WHERE
                ID = ?
        ";

        $this->query(array($statement,array($user_id)));

        return;
    }

    function load_vorlage($val_parameter=""){
        $boss_id    = $val_parameter[0];
        $vorlage_id = $val_parameter[1];

        $statement = "
            SELECT
                ID,
                BEZEICHNUNG
            FROM
                raideinteilung_vorlage_bezeichnung
            WHERE
                BOSS_ID = ?
            ORDER BY
                ID ASC
        ";

        $vorlagen = $this->select(array($statement,array($boss_id)));

        $ausgabe = '<option value="">Vorlage auswählen</option><optgroup>---------</optgroup>';
        foreach ($vorlagen as $key => $var) {
            $id = $var['ID'];
            $bezeichnung = $var['BEZEICHNUNG'];

            $ausgabe .= '<option value="'.$id.'">'.$bezeichnung.'</option>';
        }

        return $ausgabe;
    }

    function set_vorlage($val_parameter=""){
        $boss_id    = $val_parameter[0];
        $vorlage_id = $val_parameter[1];

        $statement = "
            DELETE FROM raideinteilung_boss_user_platz_zuordnung
            WHERE
                BOSS_ID = ?
        ";

        $this->query(array($statement,array($boss_id)));

        $statement = "
            INSERT INTO raideinteilung_boss_user_platz_zuordnung
            SELECT
                raideinteilung_vorlage.BOSS_ID,
                raideinteilung_vorlage.USER_ID,
                raideinteilung_vorlage.PLATZ_ID,
                raideinteilung_vorlage.SORTIERUNG
            FROM
                raideinteilung_vorlage
            WHERE
                raideinteilung_vorlage.VORLAGE_ID = ?
            AND
                raideinteilung_vorlage.USER_ID IN (
                    SELECT
                        raideinteilung_user_aktuell.ID
                    FROM
                        raideinteilung_user_aktuell
                )
        ";

        $this->query(array($statement,array($vorlage_id)));

        return;
    }

    function save_vorlage($val_parameter=""){
        $boss_id      = $val_parameter[0];
        $vorlage_name = $val_parameter[1];

        $vorlage_name = $this->utf8_to_html($vorlage_name);

        $statement = "
            INSERT INTO raideinteilung_vorlage_bezeichnung
            (
                BOSS_ID,
                BEZEICHNUNG
            )
            VALUES
            (
                ?,
                ?
            )
        ";

        $this->query(array($statement,array($boss_id,$vorlage_name)));

        $statement = "
            SELECT
                ID
            FROM
                raideinteilung_vorlage_bezeichnung
            WHERE
                BOSS_ID = ?
            AND
                BEZEICHNUNG = ?
            ORDER BY
                ID DESC
            LIMIT 1
        ";

        $vorlage = $this->select(array($statement,array($boss_id,$vorlage_name)));

        $vorlage_id = $vorlage[0]['ID'];

        $statement = "
            INSERT INTO raideinteilung_vorlage
            SELECT
                BOSS_ID,
                USER_ID,
                PLATZ_ID,
                SORTIERUNG,
                ".$vorlage_id."
            FROM
                raideinteilung_boss_user_platz_zuordnung
            WHERE
                BOSS_ID = ?
        ";

        $this->query(array($statement,array($boss_id)));

        return $vorlage_id;
    }

    function delete_vorlage($val_parameter = ""){
        $statement = "
            DELETE FROM raideinteilung_vorlage
            WHERE
                VORLAGE_ID = ?
        ";

        $this->query(array($statement,array($val_parameter)));

        $statement = "
            DELETE FROM raideinteilung_vorlage_bezeichnung
            WHERE
                ID = ?
        ";

        $this->query(array($statement,array($val_parameter)));
    }

    function gruppe_ausgabe($value=""){
        $boss_id = $value[0];
        $nur_einzelgruppe = 1;
        if(!isset($value[1])){
            $value[1] = 8;
            $nur_einzelgruppe = 0;
        }
        $max_gruppe = $value[1];
        $min_gruppe = 1;
        if($nur_einzelgruppe){
            $min_gruppe = $max_gruppe;
        }

        $einzelgruppen = '';
        for($gruppe=$min_gruppe;$gruppe<=$max_gruppe;$gruppe++){
            $style = '';
            if($gruppe>2){
                $style = 'margin-top:5px;';
            }

            $einzelgruppen .= '<div id="gruppe_'.$gruppe.'" class="div_gruppe" style="'.$style.'">'.$gruppe.'<div style="clear:both;"></div>';

            for($platz=1;$platz<=5;$platz++){
                $user_info = $this->get_char_via_gruppe_id(array($gruppe,$platz,$boss_id));
                if($user_info){
                    $user_id    = $user_info[0]['USER_ID'];
                    $user_name  = utf8_encode($user_info[0]['USERNAME']);
                    $user_farbe = $user_info[0]['FARBE'];
                    $einzelgruppen .= '<div class="gruppe_element_full">
                        <div class="gruppe_element_user" id="gruppe_item_'.$user_id.'" style="background-color:'.$user_farbe.';">'.$user_name.'</div>
                    </div>';
                }
                else{
                    $einzelgruppen .= '<div class="gruppe_element" id="grp_elem_'.$gruppe.'_'.$platz.'"></div>';
                }
            }

            $einzelgruppen .= '</div>';
        }

        if($nur_einzelgruppe){
            return $einzelgruppen;
        }

        $userliste = $this->get_gruppen_userliste($boss_id);

        $boss_auswahl = $this->get_bossauswahl($boss_id);

        $ausgabe = '<div id="dialogbox_gruppe" class="dialogbox_gruppe" style="z-index:100;width:820px;">
            <p id="dragable" class="close_dialog"><span class="x_button" onclick="close_gruppe();">X</span></p>
            <div class="gruppe_gesamt">'.$einzelgruppen.'</div>

            <div class="gruppe_userliste">'.$userliste.'</div>

            <div class="gruppe_bossliste">'.$boss_auswahl.'</div><br><button style="margin-left:10px;" onclick="set_user_via_boss_id();">Gruppeneinteilung übernehmen</button>

            <br><br><br><button style="margin-left:10px;" onclick="reset_gruppen_user();">Alle Charaktere zurücklegen</button>

            <br><br><br><button style="margin-left:10px;" id="button_grp_export" onclick="gruppe_exportieren();">CSV Export</button>

            <div style="clear:both;"></div>
        </div>';

        return $ausgabe;
    }

    function get_gruppen_userliste($boss_id = ''){
        $statement = "
            SELECT
                raideinteilung_user_aktuell.ID,
                raideinteilung_user.USERNAME,
                raideinteilung_klasse.FARBE AS KLASSE_FARBE
            FROM
                raideinteilung_user_aktuell
                INNER JOIN raideinteilung_user ON raideinteilung_user_aktuell.ID = raideinteilung_user.ID
                LEFT JOIN raideinteilung_klasse ON raideinteilung_klasse.ID = raideinteilung_user.KLASSE
            WHERE
                raideinteilung_user.SYSTEMUSER = 0
            AND
                raideinteilung_user_aktuell.ID NOT IN (
                    SELECT
                        USER_ID
                    FROM
                        raideinteilung_user_gruppe_zuordnung
                    WHERE
                        BOSS_ID = ?
                )
            ORDER BY
                raideinteilung_user.KLASSE ASC,
                raideinteilung_user.USERNAME ASC
        ";

        $userliste = $this->select(array($statement,array($boss_id)));

        $ausgabe = '';
        foreach ($userliste as $key => $var) {
            $id           = $var['ID'];
            $username     = utf8_encode($var['USERNAME']);
            $klasse_farbe = $var['KLASSE_FARBE'];

            $style = 'background-color:#D8D8D8;';
            if($klasse_farbe){
                $style = 'background-color:'.$klasse_farbe.';';
            }

            $ausgabe .= '<div class="user_box" id="gruppe_item_'.$id.'" style="color:#000;'.$style.'">'.$username.'</div>';
        }

        return $ausgabe;
    }

    function get_char_via_gruppe_id($value=''){
        $gruppe_id = $value[0];
        $platz_id  = $value[1];
        $boss_id   = $value[2];

        $statement = "
            SELECT
                raideinteilung_user_gruppe_zuordnung.USER_ID,
                raideinteilung_user.USERNAME,
                raideinteilung_klasse.FARBE
            FROM
                raideinteilung_user_gruppe_zuordnung
                INNER JOIN raideinteilung_user ON raideinteilung_user_gruppe_zuordnung.USER_ID = raideinteilung_user.ID
                INNER JOIN raideinteilung_klasse ON raideinteilung_klasse.ID = raideinteilung_user.KLASSE
            WHERE
                raideinteilung_user_gruppe_zuordnung.BOSS_ID = ?
            AND
                raideinteilung_user_gruppe_zuordnung.GRUPPE = ?
            AND
                raideinteilung_user_gruppe_zuordnung.SORTIERUNG = ?
        ";

        $userliste = $this->select(array($statement,array($boss_id,$gruppe_id,$platz_id)));

        return $userliste;
    }

    function get_class_info($value = ''){
        $boss_id = $value[0];
        $klasse  = $value[1];

        $nur_inhalt = '';
        if(isset($value[2])){
            $nur_inhalt = $value[2];
        }

        $text_speichern = $this->get_db_text("!!Speichern!!");

        $gilde_id = $this->get_gilde_id_via_token($_REQUEST['token']);

        $select_klasse = 'TANK';
        if($klasse == '2'){$select_klasse = 'HEAL';}
        if($klasse == '3'){$select_klasse = 'DD';}

        $statement = "
            SELECT
                INHALT
            FROM
                raideinteilung_klassentaktik
            WHERE
                GILDE_ID = ?
            AND
                BOSS_ID = ?
            AND
                KLASSE = ?
        ";

        $inhalt = $this->select(array($statement,array($gilde_id,$boss_id,$klasse)));

        $ausgabe = '<div style="text-align:center;font-weight:bold;">'.$select_klasse.'</div><br>';

        $text_inhalt = '';
        if($inhalt){
            $text_inhalt = $inhalt[0]['INHALT'];
        }

        if($nur_inhalt){
            return $inhalt;
        }

        if($this->editmode()){
            $ausgabe .= '<textarea cols="48" rows="11" id="class_inhalt">'.$text_inhalt.'</textarea>
            <div style="text-align:center;"><input type="button" value="'.$text_speichern.'" id="save_info_button" onclick="save_class_info('.$klasse.');"></div>';
        }
        else{
            $text_inhalt = $this->replace_symbole($text_inhalt);
            $ausgabe .= nl2br($text_inhalt);
        }

        return $ausgabe;
    }

    function save_class_info($value = ''){
        $boss_id = $value[0];
        $klasse  = $value[1];
        $inhalt  = $this->utf8_to_html($value[2]);

        $gilde_id = $this->get_gilde_id_via_token($_REQUEST['token']);

        $statement = "
            DELETE FROM raideinteilung_klassentaktik
            WHERE
                GILDE_ID = ?
            AND
                BOSS_ID = ?
            AND
                KLASSE = ?
        ";

        $this->query(array($statement,array($gilde_id,$boss_id,$klasse)));

        if($inhalt){
            $statement = "
                INSERT INTO raideinteilung_klassentaktik
                (
                    GILDE_ID,
                    BOSS_ID,
                    KLASSE,
                    INHALT
                )
                VALUES
                (
                    ?,
                    ?,
                    ?,
                    ?
                )
            ";

            $this->query(array($statement,array($gilde_id,$boss_id,$klasse,$inhalt)));
        }

        return;
    }

    function set_user_to_grp($value = ''){
        $boss_id   = $value[0];
        $user_id   = $value[1];
        $gruppe_id = $value[2];

        $user_sort = 0;

        for($i = 1; $i <= 5; $i++){
            $statement = "
                SELECT
                    ID
                FROM
                    raideinteilung_user_gruppe_zuordnung
                WHERE
                    BOSS_ID = ?
                AND
                    GRUPPE = ?
                AND
                    SORTIERUNG = ?
            ";

            $user_sort_sql = $this->select(array($statement,array($boss_id,$gruppe_id,$i)));

            if(!$user_sort_sql){
                $user_sort = $i;
                break;
            }
        }

        $statement = "
            SELECT
                GRUPPE
            FROM
                raideinteilung_user_gruppe_zuordnung
            WHERE
                BOSS_ID = ?
            AND
                USER_ID = ?
        ";

        $gruppe_data = $this->select(array($statement,array($boss_id,$user_id)));

        $ausgabe        = '';
        $ausgabe_alt    = '';
        $ausgabe_alt_id = '';
        $userliste      = '';
        if($user_sort > 0){
            $sortierung = $user_sort;
            if($sortierung < 6){
                $statement = "
                    DELETE FROM raideinteilung_user_gruppe_zuordnung
                    WHERE
                        USER_ID = ?
                    AND
                        BOSS_ID = ?
                ";

                $this->query(array($statement,array($user_id,$boss_id)));

                $statement = "
                    INSERT INTO raideinteilung_user_gruppe_zuordnung
                    (
                        USER_ID,
                        BOSS_ID,
                        GRUPPE,
                        SORTIERUNG
                    )
                    VALUES
                    (
                        ?,
                        ?,
                        ?,
                        ?
                    )
                ";

                $this->query(array($statement,array($user_id,$boss_id,$gruppe_id,$sortierung)));

                $ausgabe = $this->gruppe_ausgabe(array($boss_id,$gruppe_id));

                $userliste = $this->get_gruppen_userliste($boss_id);

                if($gruppe_data){
                    $ausgabe_alt_id = $gruppe_data[0]['GRUPPE'];
                    $this->gruppe_neu_sortieren(array($boss_id,$ausgabe_alt_id));
                    $ausgabe_alt = $this->gruppe_ausgabe(array($boss_id,$ausgabe_alt_id));
                }
            }
        }

        return array($ausgabe,$userliste,$ausgabe_alt,$ausgabe_alt_id);
    }

    function change_gruppe_user($value=''){
        $boss_id   = $value[0];
        $user_move = $value[1];
        $user_stay = $value[2];

        $statement = "
            SELECT
                GRUPPE,
                SORTIERUNG
            FROM
                raideinteilung_user_gruppe_zuordnung
            WHERE
                USER_ID = ?
            AND
                BOSS_ID = ?
        ";

        $user_data1 = $this->select(array($statement,array($user_move,$boss_id)));
        $user_data2 = $this->select(array($statement,array($user_stay,$boss_id)));

        $user_gruppe_temp2 = $user_data2[0]['GRUPPE'];
        $user_sort_temp2 = $user_data2[0]['SORTIERUNG'];

        $ausgabe1 = '';
        $user_gruppe_temp1 = '';
        $userliste = '';
        if($user_data1){
            $user_gruppe_temp1 = $user_data1[0]['GRUPPE'];
            $user_sort_temp1 = $user_data1[0]['SORTIERUNG'];

            $statement = "
                UPDATE raideinteilung_user_gruppe_zuordnung
                SET
                    GRUPPE = ?,
                    SORTIERUNG = ?
                WHERE
                    USER_ID = ?
                AND
                    BOSS_ID = ?
            ";

            $this->query(array($statement,array($user_gruppe_temp2,$user_sort_temp2,$user_move,$boss_id)));
            $this->query(array($statement,array($user_gruppe_temp1,$user_sort_temp1,$user_stay,$boss_id)));

            $ausgabe1 = $this->gruppe_ausgabe(array($boss_id,$user_gruppe_temp1));
        }else{
            $statement = "
                INSERT INTO raideinteilung_user_gruppe_zuordnung
                (
                    USER_ID,
                    BOSS_ID,
                    GRUPPE,
                    SORTIERUNG
                )
                VALUES
                (
                    ?,
                    ?,
                    ?,
                    ?
                )
            ";

            $this->query(array($statement,array($user_move,$boss_id,$user_gruppe_temp2,$user_sort_temp2)));

            $statement = "
                DELETE FROM raideinteilung_user_gruppe_zuordnung
                WHERE
                    USER_ID = ?
                AND
                    BOSS_ID = ?
            ";

            $this->query(array($statement,array($user_stay,$boss_id)));

            $userliste = $this->get_gruppen_userliste($boss_id);
        }

        $ausgabe2 = $this->gruppe_ausgabe(array($boss_id,$user_gruppe_temp2));

        return array($ausgabe1,$ausgabe2,$user_gruppe_temp1,$user_gruppe_temp2,$userliste);
    }

    function return_user_gruppe($value=''){
        $boss_id = $value[0];
        $user_id = $value[1];

        $statement = "
            SELECT
                GRUPPE
            FROM
                raideinteilung_user_gruppe_zuordnung
            WHERE
                USER_ID = ?
            AND
                BOSS_ID = ?
        ";

        $user_data = $this->select(array($statement,array($user_id,$boss_id)));

        if(!$user_data){
            return;
        }

        $gruppe_id = $user_data[0]['GRUPPE'];

        $statement = "
            DELETE FROM raideinteilung_user_gruppe_zuordnung
            WHERE
                USER_ID = ?
            AND
                BOSS_ID = ?
        ";

        $this->query(array($statement,array($user_id,$boss_id)));

        $gruppe = $this->gruppe_ausgabe(array($boss_id,$gruppe_id));

        $userliste = $this->get_gruppen_userliste($boss_id);

        return array($userliste,$gruppe,$gruppe_id);
    }

    function gruppe_neu_sortieren($value=''){
        $boss_id   = $value[0];
        $gruppe_id = $value[1];

        $statement = "
            SELECT
                USER_ID
            FROM
                raideinteilung_user_gruppe_zuordnung
            WHERE
                BOSS_ID = ?
            AND
                GRUPPE = ?
            ORDER BY
                SORTIERUNG ASC
        ";

        $user_data = $this->select(array($statement,array($boss_id,$gruppe_id)));

        $i = 1;
        foreach($user_data as $key => $var){
            $statement_update = "
                UPDATE raideinteilung_user_gruppe_zuordnung
                SET
                    SORTIERUNG = ?
                WHERE
                    USER_ID = ?
                AND
                    BOSS_ID = ?
                AND
                    GRUPPE = ?
            ";

            $this->query(array($statement_update,array($i,$var['USER_ID'],$boss_id,$gruppe_id)));

            $i++;
        }

        return;
    }

    function get_bossauswahl($boss_id=''){
        $statement = "
            SELECT
                bosse.id AS ID,
                schlachtzug.bezeichnung AS RAID,
                bosse.bezeichnung AS BOSS
            FROM
                bosse
                INNER JOIN schlachtzug ON bosse.schlachtzug_id = schlachtzug.id
                INNER JOIN raideinteilung_addons ON schlachtzug.ADDON = raideinteilung_addons.ID
            WHERE
                bosse.bezeichnung <> 'Trash'
            AND
                raideinteilung_addons.SPERRE = 0
            ORDER BY
                schlachtzug.sortierung ASC,
                bosse.SORTIERUNG ASC
        ";

        $boss_data = $this->select($statement);

        $ausgabe = '<select id="gruppe_boss_auswahl" style="width: 230px;"><option></option>';
        $raid_temp = '';
        foreach($boss_data as $key => $var){
            $boss_name = utf8_encode($var['BOSS']);
            $raid_name = utf8_encode($var['RAID']);
            $checked = ($boss_id == $var['ID'])?'selected="selected"':'';

            if($raid_temp != $var['RAID']){
                if($raid_temp){
                    $ausgabe.= '</optgroup>';
                }
                $ausgabe .= '<optgroup label="'.$raid_name.'">';
            }

            $ausgabe .= '<option value="'.$var['ID'].'" '.$checked.'>'.$boss_name.'</option>';

            $raid_temp = $var['RAID'];
        }
        $ausgabe .= '</optgroup></select>';

        return $ausgabe;
    }

    function set_user_via_boss_id($value=''){
        $boss_id      = $value[0];
        $boss_id_from = $value[1];

        if($boss_id == $boss_id_from){
            return;
        }

        $statement = "
            DELETE FROM raideinteilung_user_gruppe_zuordnung
            WHERE
                BOSS_ID = ?
        ";

        $this->query(array($statement,array($boss_id)));

        $statement = "
            INSERT INTO raideinteilung_user_gruppe_zuordnung
            SELECT
                NULL,
                USER_ID,
                ?,
                GRUPPE,
                SORTIERUNG
            FROM
                raideinteilung_user_gruppe_zuordnung
            WHERE
                BOSS_ID = ?
        ";

        $this->query(array($statement,array($boss_id,$boss_id_from)));

        return;
    }

    function reset_gruppen_user($boss_id=''){
        $statement = "
            DELETE FROM raideinteilung_user_gruppe_zuordnung
            WHERE
                BOSS_ID = ?
        ";

        $this->query(array($statement,array($boss_id)));

        return;
    }

    function get_raidplan($value=''){
        $ausgabe = '';

        $statement = "
            SELECT
                bosse.schlachtzug_id,
                raideinteilung_raidplan.BOSS_ID,
                bosse.bezeichnung,
                schlachtzug.FARBE,
                schlachtzug.ADDON
            FROM
                raideinteilung_raidplan
                INNER JOIN bosse ON raideinteilung_raidplan.BOSS_ID = bosse.id
                INNER JOIN schlachtzug ON schlachtzug.id = bosse.schlachtzug_id
            ORDER BY
                raideinteilung_raidplan.SORTIERUNG ASC
        ";

        $boss_data = $this->select($statement);

        if($this->editmode()){
            $boss_auswahl = $this->get_bossauswahl();

            foreach($boss_data as $key => $var){
                $boss_name = utf8_encode($var['bezeichnung']);
                $boss_id = $var['BOSS_ID'];
                $raid_id = $var['schlachtzug_id'];
                $farbe   = $var['FARBE'];

                $style = '';
                if($farbe){$style='style="background-color:'.$farbe.';"';}

                $ausgabe .= '<tr id="planer_row_'.$boss_id.'"><td class="raidplan_item_edit" '.$style.' id="raidplan_item_'.$boss_id.'">'.$boss_name.'</td><td><img src="delete.png" style="cursor:pointer;margin-top:4px;" onclick="delete_raidplan_item(\''.$boss_id.'\');"></td></tr>';
            }

            $ausgabe .= '<tr><td class="raidplan_item_edit">'.$boss_auswahl.'</td><td><div class="add_planer_item" onclick="add_raidplan_item();">+</div></td></tr>';

            if($ausgabe){
                $ausgabe = '<span style="font-size:20px;font-weight:bold;">Aktueller Raidplan</span><br><br><table style="margin-left:auto;margin-right:auto;" id="edit_raidplan_table">'.$ausgabe.'</table>';
            }
        }
        else{
            foreach($boss_data as $key => $var){
                $boss_name = utf8_encode($var['bezeichnung']);
                $boss_id = $var['BOSS_ID'];
                $raid_id = $var['schlachtzug_id'];
                $farbe   = $var['FARBE'];
                $addon   = $var['ADDON'];

                $style = '';
                if($farbe){$style='style="background-color:'.$farbe.';"';}

                $ausgabe .= '<tr><td onclick="load_addon_liste('.$addon.');load_boss_liste('.$raid_id.','.$boss_id.',1);" class="raidplan_item" '.$style.' id="raidplan_item_'.$boss_id.'">'.$boss_name.'</td></tr>';
            }

            if(!$ausgabe){
                $ausgabe = 'Aktuell kein Raidplan hinterlegt';
            }
            else{
                $ausgabe = '<span style="font-size:20px;font-weight:bold;">Aktueller Raidplan</span><br><br><table style="margin-left:auto;margin-right:auto;">'.$ausgabe.'</table>';
            }
        }

        return $ausgabe;
    }

    function delete_raidplan_item($boss_id=''){
        $statement = "
            DELETE FROM raideinteilung_raidplan
            WHERE
                BOSS_ID = ?
        ";

        $this->query(array($statement,array($boss_id)));
    }

    function add_raidplan_item($boss_id=''){
        $statement = "
            SELECT
                IFNULL(MAX(SORTIERUNG),0) + 1 AS SORT
            FROM
                raideinteilung_raidplan
        ";

        $sortierung = $this->select($statement);

        $statement = "
            INSERT INTO raideinteilung_raidplan
            (
                BOSS_ID,
                SORTIERUNG
            )
            VALUES
            (
                ?,
                ?
            )
        ";

        $this->query(array($statement,array($boss_id,$sortierung[0]['SORT'])));

        return;
    }

    function set_boss_change($boss_id=''){
        $statement = "
            UPDATE bosse
            SET
                LAST_CHANGE = NOW()
            WHERE
                id = ?
        ";

        $this->query(array($statement,array($boss_id)));
    }

    function get_taktik_symbole($value=''){
        $ausgabe = '';

        $ausgabe .= '<img src="'.$this->get_taktik_symbol_url('15').'" onclick="add_taktik_symbol(15);" class="taktik_hover" style="width:50px;">';
        $ausgabe .= '<img src="'.$this->get_taktik_symbol_url('16').'" onclick="add_taktik_symbol(16);" class="taktik_hover" style="width:50px;">';
        $ausgabe .= '<img src="'.$this->get_taktik_symbol_url('17').'" onclick="add_taktik_symbol(17);" class="taktik_hover" style="width:50px;">';
        $ausgabe .= '<img src="'.$this->get_taktik_symbol_url('18').'" onclick="add_taktik_symbol(18);" class="taktik_hover" style="width:50px;"><br>';

        $ausgabe .= '<img src="'.$this->get_taktik_symbol_url('3').'" onclick="add_taktik_symbol(3);" class="taktik_hover" style="width:80px;"><br>';
        $ausgabe .= '<img src="'.$this->get_taktik_symbol_url('4').'" onclick="add_taktik_symbol(4);" class="taktik_hover" style="width:120px;"><br>';
        $ausgabe .= '<img src="'.$this->get_taktik_symbol_url('5').'" onclick="add_taktik_symbol(5);" class="taktik_hover" style="width:160px;"><br>';

        $ausgabe .= '<img src="'.$this->get_taktik_symbol_url('6').'" onclick="add_taktik_symbol(6);" class="taktik_hover" style="width:80px;"><br>';
        $ausgabe .= '<img src="'.$this->get_taktik_symbol_url('7').'" onclick="add_taktik_symbol(7);" class="taktik_hover" style="width:120px;"><br>';
        $ausgabe .= '<img src="'.$this->get_taktik_symbol_url('8').'" onclick="add_taktik_symbol(8);" class="taktik_hover" style="width:160px;"><br>';

        $ausgabe .= '<img src="'.$this->get_taktik_symbol_url('9').'" onclick="add_taktik_symbol(9);" class="taktik_hover" style="height:80px;">';
        $ausgabe .= '<img src="'.$this->get_taktik_symbol_url('10').'" onclick="add_taktik_symbol(10);" class="taktik_hover" style="height:120px;">';
        $ausgabe .= '<img src="'.$this->get_taktik_symbol_url('11').'" onclick="add_taktik_symbol(11);" class="taktik_hover" style="height:160px;">';

        $ausgabe .= '<img src="'.$this->get_taktik_symbol_url('12').'" onclick="add_taktik_symbol(12);" class="taktik_hover" style="height:80px;margin-left:20px;">';
        $ausgabe .= '<img src="'.$this->get_taktik_symbol_url('13').'" onclick="add_taktik_symbol(13);" class="taktik_hover" style="height:120px;">';
        $ausgabe .= '<img src="'.$this->get_taktik_symbol_url('14').'" onclick="add_taktik_symbol(14);" class="taktik_hover" style="height:160px;"><br><br>';

        $ausgabe .= '<img src="'.$this->get_taktik_symbol_url('1').'" onclick="add_taktik_symbol(1);" class="taktik_hover" style="height:80px;">';
        $ausgabe .= '<img src="'.$this->get_taktik_symbol_url('2').'" onclick="add_taktik_symbol(2);" class="taktik_hover" style="height:80px;">';

        return $ausgabe;
    }

    function get_taktik_symbol_url($value=''){
        $url = 'symbole/';
        switch($value){
            case '1': $url .= 'taktik_kreis.png';break;
            case '2': $url .= 'taktik_kreuz.png';break;
            case '3': $url .= 'taktik_pfeil_lr_klein.png';break;
            case '4': $url .= 'taktik_pfeil_lr_mittel.png';break;
            case '5': $url .= 'taktik_pfeil_lr_gross.png';break;
            case '6': $url .= 'taktik_pfeil_rl_klein.png';break;
            case '7': $url .= 'taktik_pfeil_rl_mittel.png';break;
            case '8': $url .= 'taktik_pfeil_rl_gross.png';break;
            case '9': $url .= 'taktik_pfeil_uo_klein.png';break;
            case '10': $url .= 'taktik_pfeil_uo_mittel.png';break;
            case '11': $url .= 'taktik_pfeil_uo_gross.png';break;
            case '12': $url .= 'taktik_pfeil_ou_klein.png';break;
            case '13': $url .= 'taktik_pfeil_ou_mittel.png';break;
            case '14': $url .= 'taktik_pfeil_ou_gross.png';break;
            case '15': $url .= 'tank.png';break;
            case '16': $url .= 'heal.png';break;
            case '17': $url .= 'damage.png';break;
            case '18': $url .= 'boss.png';break;
        }

        return $url;
    }

    function get_addon_auswahl($val_parameter=""){
        $statement = "
            SELECT
                ADDON
            FROM
                raideinteilung_gilden
            WHERE
                TOKEN = ?
        ";

        $addon_inhalt = $this->select(array($statement,array($_REQUEST['token'])));

        $return = '<input type="hidden" id="default_addon" value="'.$addon_inhalt[0]['ADDON'].'" />';

        return $return;
    }

    function get_char_info($char_id=''){
        $ausgabe = '';
        if(!$this->editmode()){
            $ausgabe = $this->get_db_text("!!Eingeloggt_sein!!");
        }
        else{
            $sprache = $this->get_user_language();

            $text_speichern = $this->get_db_text("!!Speichern!!");
            $text_klasse = $this->get_db_text("!!Klasse!!");

            $statement = "
                SELECT
                    ID,
                    BEZEICHNUNG,
                    FARBE,
                    (
                        SELECT
                            ".$sprache."
                        FROM
                            raideinteilung_lingua
                        WHERE
                            ID = raideinteilung_klasse.LINGUA_ID
                    ) AS ANZEIGE
                FROM
                    raideinteilung_klasse
                ORDER BY
                    ID ASC
            ";

            $klassen = $this->select($statement);

            $statement = "
                SELECT
                    KLASSE,
                    USERNAME,
                    ID
                FROM
                    raideinteilung_user
                WHERE
                    ID = ?
            ";

            $char_info = $this->select(array($statement,array($char_id)));

            $ausgabe = '<div style="text-align:center;"><span style="font-weight:bold;"><input type="hidden" value="'.$char_info[0]['ID'].'" id="char_info_id" />'.$char_info[0]['USERNAME'].'</span><br><br>'.$text_klasse.': <select id="klassenwahl">';
            foreach($klassen as $key => $var){
                $selected = ($char_info[0]['KLASSE'] == $var['ID'])?'selected="selected"':'';

                $anzeige = ($var['ANZEIGE'])?$var['ANZEIGE']:$var['BEZEICHNUNG'];

                $ausgabe .= '<option value="'.$var['ID'].'" style="background-color:'.$var['FARBE'].'" '.$selected.'>'.$anzeige.'</option>';
            }
            $ausgabe .= '</select> <button onclick="save_char_info();">'.$text_speichern.'</button></div>';
        }

        return $ausgabe;
    }

    function save_char_info($value=''){
        if($this->editmode()){
            $char_id = $value[0];
            $klasse  = $value[1];

            $statement = "
                UPDATE raideinteilung_user
                SET
                    KLASSE = ?
                WHERE
                    ID = ?
            ";

            $this->query(array($statement,array($klasse,$char_id)));
        }

        return;
    }

    function change_prio_mode($prio_id=''){
        $ausgabe = 'Zuteilung rückgängig machen';

        if(isset($_SESSION['prio_editmode'])){
            $statement = "
                SELECT
                    eqdkp23_itemprio.given AS GIVEN
                FROM
                    eqdkp23_itemprio
                WHERE
                    eqdkp23_itemprio.id = ?
            ";

            $user_data = $this->select(array($statement,array($prio_id)));

            $given = 1;

            if($user_data[0]['GIVEN'] == 1){
                $given = 0;
                $ausgabe = 'Zuteilen';
            }

            $statement = "
                UPDATE eqdkp23_itemprio
                SET
                    eqdkp23_itemprio.given = ?
                WHERE
                    eqdkp23_itemprio.id = ?
            ";

            $this->query(array($statement,array($given,$prio_id)));
        }

        return $ausgabe;
    }

    function delete_prio_item($item_id=''){
        $check = 0;
        if(isset($_SESSION['prio_editmode'])){
            $statement = "
                DELETE FROM
                    eqdkp23_itemprio
                WHERE
                    eqdkp23_itemprio.id = ?
            ";

            $this->query(array($statement,array($item_id)));

            $check = 1;
        }

        return $check;
    }

    function loginfenster($value=''){
        $ausgabe = '';

        $ausgabe .= '<div class="loginfenster"><form action="" method="post" id="login_form" name="raidtool_login">';
        if(isset($_SESSION['token'])){
            $text_logout    = $this->get_db_text("!!Logout!!");
            $text_status    = $this->get_db_text("!!Status!!");
            $text_pw_change = $this->get_db_text("!!Passwort_aendern!!");

            $status_text = '';
            $array_user = $this->get_userdaten($_SESSION['user_id']);
            switch ($array_user['gildenstatus']) {
                case '0':$status_text = $this->get_db_text("!!Anwaerter!!");break;
                case '1':$status_text = $this->get_db_text("!!Moderator!!");break;
                case '2':$status_text = $this->get_db_text("!!Gruender!!");break;
            }

            $ausgabe .= '<table>
                <tr>
                    <td style="padding-right:20px;">Token: <span style="font-weight:bold;">'.$_SESSION['token'].'</span></td>
                    <td>'.$text_status.': <span style="font-weight:bold;">'.$status_text.'</span></td>
                </tr>
                <tr>
                    <td><div class="create_link" onclick="open_password_window();">&nbsp;'.$text_pw_change.'&nbsp;</div></td>
                    <td style=""><img src="./flaggen/'.$_SESSION['sprache'].'.png" style="cursor:pointer;" onclick="open_language_window();"></span></td>
                </tr>
            </table><br>
            <input type="hidden" name="logout" id="hidden_logout" value="1"><input type="submit" value="'.$text_logout.'">';
        }else{
            $text_username = $this->get_db_text("!!Username!!");
            $text_passwort = $this->get_db_text("!!Passwort!!");
            $text_login    = $this->get_db_text("!!Login!!");
            $text_account  = $this->get_db_text("!!Account_anlegen!!");

            $ausgabe .= '<table>
                <tr>
                    <td>'.$text_username.':</td>
                    <td><input type="text" id="login_username" name="login_username"></td>
                </tr>
                <tr>
                    <td>'.$text_passwort.':</td>
                    <td><input type="password" id="pw_edit_mode" name="pw_edit_mode"></td>
                </tr>
                <tr>
                    <td></td>
                    <td style="text-align:left;"><div class="create_link" onclick="open_register();">'.$text_account.'</div><input type="submit" id="login_button" value="'.$text_login.'"></td>
                </tr>
            </table>';
        }
        $ausgabe .= '</form></div>';

        return $ausgabe;
    }

    function register_window(){
        $statement = "
            SELECT
                ID,
                REALM
            FROM
                raideinteilung_realms
            ORDER BY
                REALM ASC
        ";

        $sql_realm = $this->select($statement);

        $realmliste = '';
        foreach($sql_realm as $key => $var){
            $realmliste .= '<option value="'.$var['ID'].'">'.utf8_encode($var['REALM']).'</option>';
        }

        $text_username     = $this->get_db_text("!!Username!!");
        $text_passwort1    = $this->get_db_text("!!Passwort!!");
        $text_passwort2    = $this->get_db_text("!!Passwort_wdhl!!");
        $text_email        = $this->get_db_text("!!email!!");
        $text_realm        = $this->get_db_text("!!realm!!");
        $text_fraktion     = $this->get_db_text("!!Fraktion!!");
        $text_gilde        = $this->get_db_text("!!Gilde!!");
        $text_registrieren = $this->get_db_text("!!Registrieren!!");
        $text_abbrechen    = $this->get_db_text("!!Abbrechen!!");
        $text_allianz      = $this->get_db_text("!!Allianz!!");
        $text_horde        = $this->get_db_text("!!Horde!!");

        $ausgabe = '
            <div class="registerfenster" id="register_window">
                <table style="text-align:left;">
                    <tr>
                        <td>'.$text_username.'</td>
                        <td><input type="text" id="register_username"></td>
                    </tr>
                    <tr>
                        <td>'.$text_passwort1.'</td>
                        <td><input type="password" id="register_pw1"></td>
                    </tr>
                    <tr>
                        <td>'.$text_passwort2.'</td>
                        <td><input type="password" id="register_pw2"></td>
                    </tr>
                    <tr>
                        <td>'.$text_email.'</td>
                        <td><input type="text" id="register_email"></td>
                    </tr>
                    <tr>
                        <td>'.$text_realm.'</td>
                        <td><select id="register_realm">'.$realmliste.'</select></td>
                    </tr>
                    <tr>
                        <td>'.$text_fraktion.'</td>
                        <td>
                            <select id="register_fraktion">
                                <option value="2">'.$text_allianz.'</option>
                                <option value="1">'.$text_horde.'</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>'.$text_gilde.'</td>
                        <td><input type="text" id="register_gilde"></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height:10px;"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button onclick="register_me();">'.$text_registrieren.'</button> <button onclick="close_register();">'.$text_abbrechen.'</button></td>
                    </tr>
                </table>
            </div>
        ';

        return $ausgabe;
    }

    function register_user($value=''){
        $username  = $value[0];
        $passwort1 = $value[1];
        $passwort2 = $value[2];
        $email     = trim($value[3]);
        $realm     = $value[4];
        $fraktion  = $value[5];
        $gilde     = trim($value[6]);

        $ausgabe = '';
        $erfolgreich = 1;
        if(!$username){
            if($ausgabe){$ausgabe .= "<br>";}
            $ausgabe .= "Bitte Username eingeben!";
            $erfolgreich = 0;
        }
        else{
            $statement = "
                SELECT
                    ID
                FROM
                    raideinteilung_userdaten
                WHERE
                    UPPER(USERNAME) = UPPER(?)
            ";

            $user_vorhanden = $this->select(array($statement,array($username)));
            if($user_vorhanden){
                $ausgabe .= "Username bereits vergeben!";
                $erfolgreich = 0;
            }
        }

        if(!$passwort1){
            if($ausgabe){$ausgabe .= "<br>";}
            $ausgabe .= "Bitte Passwort eingeben!";
            $erfolgreich = 0;
        }

        if($passwort1 != $passwort2){
            if($ausgabe){$ausgabe .= "<br>";}
            $ausgabe .= "Beide Passwörter müssen identisch sein!";
            $erfolgreich = 0;
        }

        if(!$email){
            if($ausgabe){$ausgabe .= "<br>";}
            $ausgabe .= "Bitte eMail Adresse eingeben!";
            $erfolgreich = 0;
        }
        elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            if($ausgabe){$ausgabe .= "<br>";}
            $ausgabe .= "Bitte eine korrekte eMail Adresse eingeben!";
            $erfolgreich = 0;
        }

        if(!$gilde){
            if($ausgabe){$ausgabe .= "<br>";}
            $ausgabe .= "In welcher Gilde befindest Du dich?";
            $erfolgreich = 0;
        }

        if($erfolgreich == 1){
            // Registrieren
            $statement = "
                SELECT
                    ID
                FROM
                    raideinteilung_gilden
                WHERE
                    REALM = ?
                AND
                    UPPER(GILDENNAME) = UPPER(?)
            ";

            $gilden_sql = $this->select(array($statement,array($realm,$gilde)));

            $gilde_id = 0;
            if($gilden_sql){
                $gilde_id = $gilden_sql[0]['ID'];
            }

            $gildenstatus = 0;
            if(!$gilde_id){
                $token = $this->get_token();

                $statement = "
                    INSERT INTO raideinteilung_gilden
                    (
                        GILDENNAME,
                        REALM,
                        TOKEN,
                        FRAKTION
                    )
                    VALUES
                    (
                        ?,
                        ?,
                        ?,
                        ?
                    )
                ";

                $this->query(array($statement,array($gilde,$realm,$token,$fraktion)));

                $statement = "
                    SELECT
                        Max(ID) AS LAST_ID
                    FROM
                        raideinteilung_gilden
                ";

                $sql_id = $this->select($statement);

                $gilde_id = $sql_id[0]['LAST_ID'];

                $statement = "
                    INSERT INTO raideinteilung_user (USERNAME,SYSTEMUSER,GILDE) VALUES
                        ('<img data-sort=\"1\" src=\"symbole/totenkopf.png\"> Totenkopf',1,?),
                        ('<img data-sort=\"2\" src=\"symbole/kreuz.png\"> Kreuz',1,?),
                        ('<img data-sort=\"3\" src=\"symbole/stern.png\"> Stern',1,?),
                        ('<img data-sort=\"4\" src=\"symbole/quadrat.png\"> Quadrat',1,?),
                        ('<img data-sort=\"5\" src=\"symbole/mond.png\"> Mond',1,?),
                        ('<img data-sort=\"6\" src=\"symbole/dreieck.png\"> Dreieck',1,?),
                        ('<img data-sort=\"7\" src=\"symbole/diamant.png\"> Diamant',1,?),
                        ('<img data-sort=\"8\" src=\"symbole/kreis.png\"> Kreis',1,?)
                ";

                $this->query(array($statement,array($gilde_id,$gilde_id,$gilde_id,$gilde_id,$gilde_id,$gilde_id,$gilde_id,$gilde_id)));

                $gildenstatus = 2;
            }

            $statement = "
                INSERT INTO raideinteilung_userdaten
                (
                    USERNAME,
                    PASSWORT,
                    SALT,
                    GILDE,
                    EMAIL,
                    GILDENSTATUS,
                    AKTIVIERUNGSLINK,
                    SPRACHE
                )
                VALUES
                (
                    ?,
                    ?,
                    ?,
                    ?,
                    ?,
                    ?,
                    ?,
                    'DE'
                )
            ";

            $salt = $this->get_random_string();

            $av_link = sha1(date("d.m.Y H:i:s").$this->get_random_string());

            $this->query(array($statement,array($username,sha1($passwort1.$salt),$salt,$gilde_id,$email,$gildenstatus,$av_link)));

            $mailtext = 'Hallo '.$username.'<br><br>
            Nutze folgenden Link, um deine Registrierung im Raidtool zu best&auml;tigen:<br><br>
            <a href="https://raidtool.seidig-glaenzend.de/?activate='.$av_link.'" target="_blank">'.$av_link.'</a>';

            $this->send_mail(array($email,'Raidtool - Registrierung',$mailtext));
        }

        return array($ausgabe,$erfolgreich);
    }

    function get_token(){
        $token = $this->get_random_string();

        $statement = "
            SELECT
                COUNT(*) AS ANZAHL
            FROM
                raideinteilung_gilden
            WHERE
                TOKEN = ?
        ";

        $sql_token = $this->select(array($statement,array($token)));

        if($sql_token[0]['ANZAHL'] > 0){
            return $this->get_token();
        }
        else{
            return $token;
        }
    }

    function get_random_string(){
        $w_s = array('A','B','C','D','E','F','G','H','J','K','M','N','P','Q','R','S','T','U','V','W','X','Y','Z');
        $max = count($w_s) - 1;
        $string = "";
        for($i=1;$i<=5;$i++){
            srand((double)microtime()*1000000);
            $wg = rand(0,$max);
            $string .= $w_s[$wg];
        }

        return $string;
    }

    function check_login($value=''){
        $username = $_REQUEST['login_username'];
        $passwort = $_REQUEST['pw_edit_mode'];

        $ergebnis      = 0;
        $token         = '';
        $gruppenplaner = 0;
        $raidplan      = 0;
        $vorlage       = 0;
        $gilde_id      = 0;
        $gildenstatus  = 0;
        $user_id       = 0;
        $fraktion      = 0;
        $sprache       = '';

        $statement = "
            SELECT
                PASSWORT,
                SALT,
                TOKEN,
                GRUPPENPLANER,
                RAIDPLAN,
                VORLAGEN,
                raideinteilung_gilden.ID AS GILDE_ID,
                GILDENSTATUS,
                raideinteilung_userdaten.ID,
                FRAKTION,
                SPRACHE
            FROM
                raideinteilung_userdaten
                INNER JOIN raideinteilung_gilden ON raideinteilung_userdaten.GILDE = raideinteilung_gilden.ID
            WHERE
                USERNAME = ?
            AND
                AKTIV = 1
        ";

        $userdaten = $this->select(array($statement,array($username)));

        if($userdaten){
            $check_pw = sha1($passwort.$userdaten[0]['SALT']);

            if($check_pw == $userdaten[0]['PASSWORT']){
                $ergebnis      = 1;
                $token         = $userdaten[0]['TOKEN'];
                $gruppenplaner = $userdaten[0]['GRUPPENPLANER'];
                $vorlage       = $userdaten[0]['VORLAGEN'];
                $gilde_id      = $userdaten[0]['GILDE_ID'];
                $gildenstatus  = $userdaten[0]['GILDENSTATUS'];
                $user_id       = $userdaten[0]['ID'];
                $fraktion      = $userdaten[0]['FRAKTION'];
                $sprache       = $userdaten[0]['SPRACHE'];

                if(isset($_COOKIE)){
                    if(isset($_COOKIE['raidtool'])){
                        if($sprache != $_COOKIE['raidtool']){
                            $sprache = $_COOKIE['raidtool'];

                            $statement = "
                                UPDATE raideinteilung_userdaten
                                SET
                                    SPRACHE = ?
                                WHERE
                                    ID = ?
                            ";

                            $this->query(array($statement,array($sprache,$user_id)));
                        }
                    }
                }
            }
        }

        $array['ergebnis']      = $ergebnis;
        $array['token']         = $token;
        $array['gruppenplaner'] = $gruppenplaner;
        $array['raidplan']      = $raidplan;
        $array['vorlage']       = $vorlage;
        $array['gilde']         = $gilde_id;
        $array['gildenstatus']  = $gildenstatus;
        $array['user_id']       = $user_id;
        $array['fraktion']      = $fraktion;
        $array['sprache']       = $sprache;

        return $array;
    }

    function check_token($token=''){
        $statement = "
            SELECT
                Count(ID) AS ANZAHL
            FROM
                raideinteilung_gilden
            WHERE
                TOKEN = ?
        ";

        $userdaten = $this->select(array($statement,array($token)));

        $ergebnis = 0;
        if($userdaten){
            $ergebnis = $userdaten[0]['ANZAHL'];
        }

        return $ergebnis;
    }

    function get_app_via_name($app_name=''){
        $spalte = '';
        switch ($app_name) {
            case strtolower('gruppenplaner'):$spalte = 'GRUPPENPLANER';break;
            case strtolower('raidplan'):$spalte = 'RAIDPLAN';break;
            case strtolower('vorlagen'):$spalte = 'VORLAGEN';break;
        }

        $statement = "
            SELECT
                ".$spalte."
            FROM
                raideinteilung_gilden
            WHERE
                TOKEN = ?
        ";

        $appdaten = $this->select(array($statement,array($_REQUEST['token'])));

        $ergebnis = 0;
        if($appdaten){
            $ergebnis = $appdaten[0][$spalte];
        }

        return $ergebnis;
    }

    function editmode($value=''){
        $ergebnis = 0;
        if(isset($_SESSION['token']) and isset($_REQUEST['token'])){
            if($_SESSION['token'] == $_REQUEST['token'] && $_SESSION['gildenstatus'] > 0){
                $ergebnis = 1;
            }
        }

        return $ergebnis;
    }

    function get_gilde_id_via_token($token=''){
        $statement = "
            SELECT
                ID
            FROM
                raideinteilung_gilden
            WHERE
                TOKEN = ?
        ";

        $gildendaten = $this->select(array($statement,array($token)));

        $ergebnis = 0;
        if($gildendaten){
            $ergebnis = $gildendaten[0]['ID'];
        }

        return $ergebnis;
    }

    function get_memberliste($token=''){
        $ausgabe = $this->get_db_text("!!keine_Berechtigung!!");

        if($_SESSION['gildenstatus'] == 2){
            $text_addon_auswahl         = $this->get_db_text("!!Addon_auswahl!!");
            $text_auswahl_speichern     = $this->get_db_text("!!Auswahl_speichern!!");
            $text_userverwaltung        = $this->get_db_text("!!Userverwaltung!!");
            $text_darf_bearbeiten       = $this->get_db_text("!!Darf_bearbeiten!!");
            $text_darf_nicht_bearbeiten = $this->get_db_text("!!Darf_nicht_bearbeiten!!");
            $text_aendern               = $this->get_db_text("!!Aendern!!");
            $text_recht_uebertrag       = $this->get_db_text("!!Recht_uebertragen!!");

            $ausgabe = '<table style="text-align:left;margin-left:auto;margin-right:auto;">
                <tr><td colspan="3" style="font-weight:bold;text-align:center;border-bottom:1px solid #000000;">'.$text_addon_auswahl.'</td><tr>
            ';

            $statement = "
                SELECT
                    raideinteilung_addons.ID AS ADDON_ID,
                    raideinteilung_addons.ADDON AS ADDON,
                    (
                        SELECT
                            raideinteilung_gilden.ADDON
                        FROM
                            raideinteilung_gilden
                        WHERE
                            raideinteilung_gilden.TOKEN = ?
                    ) AS GILDE_ADDON_ID
                FROM
                    raideinteilung_addons
                WHERE
                    raideinteilung_addons.SPERRE = 0
                ORDER BY
                    raideinteilung_addons.SORTIERUNG ASC
            ";

            $addons = $this->select(array($statement,array($_SESSION['token'])));

            $ausgabe .= '<tr><td colspan="3"><select id="addon_auswahl">';
            foreach ($addons as $key => $value) {
                $select = ($value['ADDON_ID'] == $value['GILDE_ADDON_ID'])?'selected="selected"':'';
                $ausgabe .= '<option value="'.$value['ADDON_ID'].'" '.$select.'>'.$value['ADDON'].'</option>';
            }
            $ausgabe .= '</select> <button onclick="addon_auswahl_speichern();" id="addon_auswahl_button">'.$text_auswahl_speichern.'</button></td></tr>';

            $ausgabe .= '<tr><td colspan="3" style="height:30px;"></td></tr>';

            $statement = "
                SELECT
                    ID,
                    USERNAME,
                    GILDENSTATUS
                FROM
                    raideinteilung_userdaten
                WHERE
                    GILDENSTATUS < 2
                AND
                    GILDE = ?
                ORDER BY
                    USERNAME ASC
            ";

            $userdaten = $this->select(array($statement,array($_SESSION['gilde'])));

            $ausgabe .= '<tr><td colspan="3" style="font-weight:bold;text-align:center;border-bottom:1px solid #000000;">'.$text_userverwaltung.' ['.count($userdaten).']</td></tr>';

            foreach ($userdaten as $key => $value) {
                $status_text = '<span style="color:#FA421C;font-weight:bold;">'.$text_darf_nicht_bearbeiten.'</span>';
                if($value['GILDENSTATUS'] == 1){
                    $status_text = '<span style="color:#44C050;font-weight:bold;">'.$text_darf_bearbeiten.'</span>';
                }
                $ausgabe .= '<tr><td><img src="./symbole/krone.png" style="height:15px;cursor:pointer;" onclick="recht_uebertragen('.$value['ID'].');" onmouseover="Tip(\''.$text_recht_uebertrag.'\')" onmouseout="UnTip()" /> '.$value['USERNAME'].'</td><td id="spalte_'.$value['ID'].'">'.$status_text.'</td><td><button onclick="change_user_status(\''.$value['ID'].'\');">'.$text_aendern.'</button></td></tr>';
            }

            $ausgabe .= "</table>";
        }

        return $ausgabe;
    }

    function change_user_status($user_id=''){
        $statement = "
            SELECT
                GILDE,
                GILDENSTATUS
            FROM
                raideinteilung_userdaten
            WHERE
                ID = ?
        ";

        $userdaten = $this->select(array($statement,array($user_id)));

        $ausgabe = '';
        if($userdaten){
            if($_SESSION['gilde'] == $userdaten[0]['GILDE']){
                $text_darf_bearbeiten = $this->get_db_text("!!Darf_bearbeiten!!");
                $text_darf_nicht_bearbeiten = $this->get_db_text("!!Darf_nicht_bearbeiten!!");

                $neuer_status = 1;
                $ausgabe = '<span style="color:#44C050;font-weight:bold;">'.$text_darf_bearbeiten.'</span>';
                if($userdaten[0]['GILDENSTATUS'] == 1){
                    $neuer_status = 0;
                    $ausgabe = '<span style="color:#FA421C;font-weight:bold;">'.$text_darf_nicht_bearbeiten.'</span>';
                }

                $statement = "
                    UPDATE raideinteilung_userdaten SET
                        GILDENSTATUS = ?
                    WHERE
                        ID = ?
                ";

                $this->query(array($statement,array($neuer_status,$user_id)));
            }
        }

        return $ausgabe;
    }

    function get_userdaten($user_id=''){
        $statement = "
            SELECT
                GILDE,
                GILDENSTATUS
            FROM
                raideinteilung_userdaten
            WHERE
                ID = ?
        ";

        $userdaten = $this->select(array($statement,array($user_id)));

        $array_ausgabe = array();
        if(isset($userdaten)){
            $array_ausgabe = array(
                'gilde' => $userdaten[0]['GILDE'],
                'gildenstatus' => $userdaten[0]['GILDENSTATUS']
            );
        }

        return $array_ausgabe;
    }

    function get_gildendaten_via_token($token=''){
        $statement = "
            SELECT
                raideinteilung_gilden.GILDENNAME AS GILDENNAME,
                raideinteilung_realms.REALM AS REALM,
                raideinteilung_gilden.FRAKTION AS FRAKTION
            FROM
                raideinteilung_gilden
                INNER JOIN raideinteilung_realms ON raideinteilung_gilden.REALM = raideinteilung_realms.ID
            WHERE
                raideinteilung_gilden.TOKEN = ?
        ";

        $gildendaten = $this->select(array($statement,array($token)));

        $gildenname = "";
        $realm      = "";
        $fraktion   = "";
        if($gildendaten){
            $gildenname = $gildendaten[0]['GILDENNAME'];
            $realm      = $gildendaten[0]['REALM'];
            $fraktion   = $gildendaten[0]['FRAKTION'];
        }

        return array($gildenname,$realm,$fraktion);
    }

    function addon_auswahl_speichern($addon=''){
        $statement = "
            UPDATE
                raideinteilung_gilden
            SET
                ADDON = ?
            WHERE
                ID = ?
        ";

        $this->query(array($statement,array($addon,$_SESSION['gilde'])));

        return;
    }

    function get_fraktion_via_token($token=''){
        $statement = "
            SELECT
                FRAKTION
            FROM
                raideinteilung_gilden
            WHERE
                TOKEN = ?
        ";

        $fraktion_info = $this->select(array($statement,array($token)));

        $ausgabe = 0;
        if($fraktion_info){
            $ausgabe = $fraktion_info[0]['FRAKTION'];
        }

        return $ausgabe;
    }

    function save_own_boss_img($value=''){
        $boss_img  = $value[0];
        $boss_id   = $value[1];

        if(!$boss_img){
            $statement = "
                DELETE FROM bosse_bilder
                WHERE
                    GILDE_ID = ?
                AND
                    BOSS_ID = ?
            ";

            $this->query(array($statement,array($_SESSION['gilde'],$boss_id)));
        }
        else{
            $statement = "
                INSERT INTO bosse_bilder
                (
                    GILDE_ID,
                    BOSS_ID,
                    BILD_URL
                )
                VALUES
                (
                    ?,
                    ?,
                    ?
                )
                ON DUPLICATE KEY UPDATE
                    BILD_URL = VALUES(BILD_URL)
            ";

            $this->query(array($statement,array($_SESSION['gilde'],$boss_id,$boss_img)));
        }
    }

    function bild_melden($value=''){
        $boss_id = $value[0];
        $token   = $value[1];

        $statement = '
            SELECT
                raideinteilung_gilden.GILDENNAME,
                raideinteilung_realms.REALM,
                bosse_bilder.BILD_URL,
                DATE_FORMAT(NOW(),"%d.%m.%Y %H:%i") AS UHRZEIT
            FROM
                raideinteilung_gilden
                INNER JOIN raideinteilung_realms ON raideinteilung_gilden.REALM = raideinteilung_realms.ID
                INNER JOIN bosse_bilder ON bosse_bilder.GILDE_ID = raideinteilung_gilden.ID
            WHERE
                raideinteilung_gilden.TOKEN = ?
            AND
                bosse_bilder.BOSS_ID = ?
        ';

        $meldung_info = $this->select(array($statement,array($token,$boss_id)));

        if($meldung_info){
            $gildenname = $meldung_info[0]['GILDENNAME'];
            $realm      = $meldung_info[0]['REALM'];
            $bild_url   = $meldung_info[0]['BILD_URL'];
            $uhrzeit    = $meldung_info[0]['UHRZEIT'];

            $mailtext = 'Folgendes Bild wurde im Raidtool als anstößig gemeldet:<br>';
            $mailtext .= '<br>Uhrzeit: '.$uhrzeit.' Uhr';
            $mailtext .= '<br>Gilde: '.$gildenname;
            $mailtext .= '<br>Realm: '.$realm;
            $mailtext .= '<br>URL: '.$bild_url;

            $this->send_mail(array('joerg.haas84@gmail.com','Raidtool - Anstößiges Bild',$mailtext));
        }

        return;
    }

    function get_user_aktiviert($value=''){
        $statement = "
            SELECT
                ID,
                GILDE,
                GILDENSTATUS
            FROM
                raideinteilung_userdaten
            WHERE
                AKTIVIERUNGSLINK = ?
            AND
                AKTIV = 0
        ";

        $user_id = $this->select(array($statement,array($value)));

        $ausgabe = 0;
        if($user_id){
            $statement = "
                UPDATE raideinteilung_userdaten
                SET
                    AKTIV = 1
                WHERE
                    ID = ?
            ";

            $this->query(array($statement,array($user_id[0]['ID'])));

            if($user_id[0]['GILDENSTATUS'] < 2){
                $statement = "
                    SELECT
                        USERNAME,
                        EMAIL
                    FROM
                        raideinteilung_userdaten
                    WHERE
                        GILDENSTATUS = 2
                    AND
                        GILDE = ?
                ";

                $gilde_chef = $this->select(array($statement,array($user_id[0]['GILDE'])));

                $mailtext = 'Hallo '.$gilde_chef[0]['USERNAME'].'<br><br>
                Ein neues Gildenmitglied hat sich soeben im Raidtool zu deiner Gilde registriert.<br>
                Evtl. br&auml;uchte es schreibrechte. Bitte &uuml;berpr&uuml;fen unter "Verwaltung".';

                $this->send_mail(array($gilde_chef[0]['EMAIL'],'Raidtool - Neues Gildenmitglied',$mailtext));
            }

            $ausgabe = 1;
        }

        return $ausgabe;
    }

    function remove_url_param( $url, $param ) {
        $base_url   = strtok($url, '?');             // Get the base url
        $parsed_url = parse_url($url);               // Parse it
        $query      = $parsed_url['query'];          // Get the query string

        parse_str( $query, $parameters );            // Convert Parameters into array

        unset( $parameters[$param] );                // Delete the one you want

        $new_query  = http_build_query($parameters); // Rebuilt query string

        $ausgabe = $base_url;
        if($new_query){
            $ausgabe .= '?'.$new_query;
        }

        return $ausgabe;
    }

    function send_mail($val_parameter=""){
        $mail_empfaenger = $val_parameter[0];
        $betreff         = $val_parameter[1];
        $text            = $val_parameter[2];
        $mail_absender   = "raidtool@seidig-glaenzend.de";

        mail($mail_empfaenger, $betreff, $text, "from:$mail_absender\r\nContent-Type:text/html;charset=UTF-8\r\nContent-Transfer-Encoding: 8bit\r\n");
    }

    function get_db_text($lingua_key=''){
        $sprache = 'DE';
        if(isset($_SESSION['sprache']) and $_SESSION['sprache'] != $sprache){
            $sprache = $_SESSION['sprache'];
        }
        elseif(isset($_COOKIE)){
            if(isset($_COOKIE['raidtool'])){
                $sprache = $_COOKIE['raidtool'];
            }
        }

        $statement = "
            SELECT
                ".$sprache."
            FROM
                raideinteilung_lingua
            WHERE
                LINGUA_KEY = ?
        ";

        $lingua_ref = $this->select(array($statement,array($lingua_key)));

        if($lingua_ref){
            if(!$lingua_ref[0][$sprache]){
                $sprache = "DE";
                $statement = "
                    SELECT
                        ".$sprache."
                    FROM
                        raideinteilung_lingua
                    WHERE
                        LINGUA_KEY = ?
                ";

                $lingua_ref = $this->select(array($statement,array($lingua_key)));
            }
        }

        $ausgabe = '=='.$lingua_key.'==';
        if($lingua_ref){
            if($_SERVER['HTTP_HOST'] == "localhost"){
                $ausgabe = $lingua_ref[0][$sprache];
            }
            else{
                $ausgabe = utf8_encode($lingua_ref[0][$sprache]);
            }
        }

        return $ausgabe;
    }

    function get_language($typ=''){
        $db_zugang = $this->get_db_zugangsdaten();
        $dbname = $db_zugang['name'];

        $statement = "
            SELECT
                COLUMN_NAME
            FROM
                INFORMATION_SCHEMA.COLUMNS
            WHERE
                TABLE_SCHEMA = '".$dbname."'
            AND
                TABLE_NAME = 'raideinteilung_lingua'
            AND
                COLUMN_NAME NOT IN ('ID','LINGUA_KEY')
        ";

        $ref_language = $this->select($statement);

        $ausgabe = '';
        if(!$typ){
            $ausgabe .= $this->get_db_text("!!Sprache_waehlen!!").'<br><br>';
        }

        foreach($ref_language AS $key => $value){
            $bild_zusatz = ($typ)?'':'_big';
            $ausgabe .= '<img src="./flaggen/'.$value['COLUMN_NAME'].$bild_zusatz.'.png" class="language_auswahl" onclick="select_language(\''.$value['COLUMN_NAME'].'\');" />';
        }

        return $ausgabe;
    }

    function select_language($sprache=''){
        if(isset($_SESSION['user_id'])){
            $statement = "
                UPDATE raideinteilung_userdaten
                SET
                    SPRACHE = ?
                WHERE
                    ID = ?
            ";

            $this->query(array($statement,array($sprache,$_SESSION['user_id'])));

            $_SESSION['sprache'] = $sprache;

            setcookie("raidtool",$sprache,time()+(3600*24*30));
        }
        else{
            setcookie("raidtool",$sprache,time()+(3600*24*30));
        }

        return 1;
    }

    function get_user_language($value=''){
        $sprache = 'DE';
        if(isset($_SESSION['sprache'])){
            if(isset($_SESSION['sprache'])){
                $sprache = $_SESSION['sprache'];
            }
        }
        elseif(isset($_COOKIE['raidtool'])){
            $sprache = $_COOKIE['raidtool'];
        }

        return $sprache;
    }

    function recht_uebertragen($target_id=''){
        $fehler = $this->get_db_text("!!Falscher_Gildenstatus!!");

        if($this->get_gildenstatus_via_userid($_SESSION['user_id']) == 2){
            $fehler = '';

            $statement = "
                UPDATE raideinteilung_userdaten
                SET
                    GILDENSTATUS = 2
                WHERE
                    ID = ?
            ";

            $this->query(array($statement,array($target_id)));

            $statement = "
                UPDATE raideinteilung_userdaten
                SET
                    GILDENSTATUS = 1
                WHERE
                    ID = ?
            ";

            $this->query(array($statement,array($_SESSION['user_id'])));

            $_SESSION['gildenstatus'] = 1;

            $statement = "
                SELECT
                    USERNAME,
                    EMAIL
                FROM
                    raideinteilung_userdaten
                WHERE
                    ID = ?
            ";

            $ref_userdaten = $this->select(array($statement,array($target_id)));

            $mailtext = 'Hallo '.$ref_userdaten[0]['USERNAME'].'<br><br>
            Dir wurden soeben die Gildengründungsrechte im Raidtool übertragen.';

            $this->send_mail(array($ref_userdaten[0]['EMAIL'],'Raidtool - Rechte wurden übertragen',$mailtext));
        }

        return $fehler;
    }

    function get_gildenstatus_via_userid($userid){
        $statement = "
            SELECT
                GILDENSTATUS
            FROM
                raideinteilung_userdaten
            WHERE
                ID = ?
        ";

        $ref_status = $this->select(array($statement,array($userid)));

        $gildenstatus = 0;
        if($ref_status){
            $gildenstatus = $ref_status[0]['GILDENSTATUS'];
        }

        return $gildenstatus;
    }

    function change_password($value=''){
        $pw1 = $value[0];
        $pw2 = $value[1];
        $pw3 = $value[2];

        $fehler = '';
        if(!$pw1 or !$pw2 or !$pw3){
            $fehler = $this->get_db_text("!!Passwort_ist_leer!!");
        }
        else{
            $statement = "
                SELECT
                    PASSWORT,
                    SALT
                FROM
                    raideinteilung_userdaten
                WHERE
                    ID = ?
            ";

            $ref_passwort = $this->select(array($statement,array($_SESSION['user_id'])));

            if(sha1($pw1.$ref_passwort[0]['SALT']) == $ref_passwort[0]['PASSWORT']){
                if($pw2 == $pw3){
                    $passwort_save = sha1($pw2.$ref_passwort[0]['SALT']);

                    $statement = "
                        UPDATE raideinteilung_userdaten
                        SET
                            PASSWORT = ?
                        WHERE
                            ID = ?
                    ";

                    $this->query(array($statement,array($passwort_save,$_SESSION['user_id'])));
                }
                else{
                    $fehler = $this->get_db_text("!!Passwortfehler_2!!");
                }
            }
            else{
                $fehler = $this->get_db_text("!!Passwortfehler_3!!");
            }
        }

        if($fehler){
            $fehler .= '<br><br>';
        }

        return $fehler;
    }
}
$db = new datenbank();
?>
