<?php
    include("db_lib.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
<head>
    <title>Raidtool - FAQ</title>
    <link href="default.css" type="text/css" rel="stylesheet" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script type="text/javascript" src="jquery-1.11.1.js"></script>
    <script type="text/javascript" src="jquery-ui.js"></script>
    <script type="text/javascript" src="default.js"></script>
    <link rel="icon" type="image/vnd.microsoft.icon" href="favicon.ico">
</head>
<body>
<?php

$ausgabe = '';

$statement = "
    SELECT
        ID,
        FRAGE,
        ANTWORT
    FROM
        raideinteilung_faq
    WHERE
        FRAGE IS NOT NULL
    AND
        SPERRE = 0
    ORDER BY
        SORTIERUNG ASC
";

$faq_ref = $db->select($statement);

$ausgabe .= '<div class="faq_main">';

foreach($faq_ref as $key => $value){
    $ausgabe .= '<div id="frage_'.$value['ID'].'" class="faq_frage" onclick="toggle_antwort('.$value['ID'].');">'.$value['FRAGE'].'</div>';
    $ausgabe .= '<div id="antwort_'.$value['ID'].'" class="faq_antwort">'.$value['ANTWORT'].'</div>';
}

$ausgabe .= '</div>';

print $ausgabe;
?>

</body>
</html>
