<?php
    session_start();

    if(isset($_REQUEST['pw_edit_mode'])){
        if(sha1($_REQUEST['pw_edit_mode']) == '7e87144c8a76865f63f4aad6d78eec9734d36fe5'){ # admin_prio
            $_SESSION['prio_editmode'] = 1;
        }
    }

    if(isset($_REQUEST['logout'])){
        unset($_SESSION['prio_editmode']);
    }

    include("db_lib.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
    <head>
        <title>Prio Vergabe</title>
        <link rel="icon" type="image/vnd.microsoft.icon" href="favicon.ico">
        <script type="text/javascript" src="jquery-1.11.1.js"></script>
        <script type="text/javascript" src="jquery-ui.js"></script>
        <script type="text/javascript" src="default.js"></script>
        <style type="text/css">
            body{
                font-family: arial,arial;
            }

            .loginfenster{
                float: right;
                text-align: center;
                padding: 5px;
                background-color: #E6E6E6;
                border: 2px solid #000;
                border-radius: 5px;
            }
        </style>
    </head>
    <body>
<?php
    $ausgabe = '<div class="loginfenster"><form action="" method="post" id="login_form">Bearbeitungsmodus: ';
    if(isset($_SESSION['prio_editmode'])){
        $ausgabe .= '<span style="font-weight:bold;">AN</span><br><input type="hidden" name="logout" id="hidden_logout" value="1"><input type="submit" value="Logout">';
    }else{
        $ausgabe .= '<span style="font-weight:bold;">AUS</span><br><input type="password" name="pw_edit_mode"> <input type="submit" value="Login">';
    }
    $ausgabe .= '</form></div>';

    print $ausgabe;

    $statement = "
        SELECT
            eqdkp23_config.config_value AS EVENTS
        FROM
            eqdkp23_config
        WHERE
            eqdkp23_config.config_name = 'events'
    ";

    $config = $db->select($statement);

    $events = $config[0]['EVENTS'];

    preg_match_all("/\"(\d{1,2})\"/", $events, $matches, PREG_SET_ORDER);

    $liste = '';
    foreach ($matches as $key => $value) {
        if($liste){$liste .= ',';}
        $liste .= $value[1];
    }

    if($liste){
        $statement = "
            SELECT
                eqdkp23_events.event_name AS EVENT_NAME,
                eqdkp23_events.event_id AS EVENT_ID
            FROM
                eqdkp23_events
            WHERE
                eqdkp23_events.event_id IN (".$liste.")
            ORDER BY
                eqdkp23_events.event_name ASC
        ";

        $raids = $db->select($statement);

        foreach ($raids as $key => $value) {
            print '<a href="prio_vergabe.php?raidauswahl='.$value['EVENT_ID'].'" style="margin: 3px 10px;">'.utf8_encode($value['EVENT_NAME']).'</a>';
        }
    }

    if(isset($_REQUEST['raidauswahl'])){
        $statement = "
            SELECT
                eqdkp23_itemprio.id AS PRIO_ID,
                eqdkp23_members.member_name AS MEMBER,
                eqdkp23_itemprio.itemname AS PRIO_ITEM,
                eqdkp23_itemprio.memberid AS MEMBER_ID,
                eqdkp23_itemprio.given AS VERGEBEN
            FROM
                eqdkp23_itemprio
                INNER JOIN eqdkp23_members ON eqdkp23_members.member_id = eqdkp23_itemprio.memberid
            WHERE
                eqdkp23_itemprio.eventid = ?
            AND
                eqdkp23_itemprio.itemname <> ''
            ORDER BY
                eqdkp23_members.member_name ASC,
                eqdkp23_itemprio.prio ASC
        ";

        $user_data = $db->select(array($statement,array($_REQUEST['raidauswahl'])));

        $temp = '';
        print '<hr><table style="margin-top:20px;">
            <tr>
                <td>Charakter</td>
                <td>Prio Item</td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3"><hr></td>
            </tr>
        <tr>';

        foreach($user_data as $key => $var){

            if($temp and $temp != $var['MEMBER_ID']){
                print '</tr><tr><td colspan="3"><hr></td></tr><tr>';
            }
            else{
                print '</tr><tr>';
            }

            $delete_button = '';
            if(isset($_SESSION['prio_editmode'])){
                $delete_button = '<button onclick="delete_prio_item(\''.$var['PRIO_ID'].'\');" style="color:#ff0000;"> X </button> ';
            }

            print '<td>'.$delete_button.'<span id="charakter_'.$var['PRIO_ID'].'">'.utf8_encode($var['MEMBER']).'</span></td>';
            print '<td id="item_'.$var['PRIO_ID'].'">'.utf8_encode($var['PRIO_ITEM']).'</td>';

            if(isset($_SESSION['prio_editmode'])){
                $button_text = ($var['VERGEBEN'] == 1)?'Zuteilung rückgängig machen':'Zuteilen';

                print '<td><button id="button_'.$var['PRIO_ID'].'" onclick="change_prio_mode(\''.$var['PRIO_ID'].'\');">'.$button_text.'</button></td>';
            }

            $temp = $var['MEMBER_ID'];
        }
        print '</tr></table>';
    }
?>
    </body>
</html>
