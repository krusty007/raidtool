<?php
session_start();
if (is_ajax()) {
    if (isset($_POST["action"]) && !empty($_POST["action"])) { //Checks if action value exists
        $action = $_POST["action"];
        switch($action) { //Switch case for value of action
            case "get_boss_liste": get_boss_liste(); break;
            case "import_user": import_user(); break;
            case "add_area": add_area(); break;
            case "save_area_position": save_area_position(); break;
            case "load_area": load_area(); break;
            case "delete_area": delete_area(); break;
            case "edit_text_area": edit_text_area(); break;
            case "load_userliste": load_userliste(); break;
            case "drop_user": drop_user(); break;
            case "reset_user": reset_user(); break;
            case "clear_userliste": clear_userliste(); break;
            case "sort_down": sort_down(); break;
            case "sort_up": sort_up(); break;
            case "get_planungsmodus": get_planungsmodus(); break;
            case "set_planungsmodus": set_planungsmodus(); break;
            case "delete_user": delete_user(); break;
            case "load_vorlage": load_vorlage(); break;
            case "set_vorlage": set_vorlage(); break;
            case "save_vorlage": save_vorlage(); break;
            case "delete_vorlage": delete_vorlage(); break;
            case "get_class_info": get_class_info(); break;
            case "save_class_info": save_class_info(); break;
            case "gruppe_ausgabe": gruppe_ausgabe(); break;
            case "set_user_to_grp": set_user_to_grp(); break;
            case "change_gruppe_user": change_gruppe_user(); break;
            case "return_user_gruppe": return_user_gruppe(); break;
            case "set_user_via_boss_id": set_user_via_boss_id(); break;
            case "reset_gruppen_user": reset_gruppen_user(); break;
            case "get_raidplan": get_raidplan(); break;
            case "delete_raidplan_item": delete_raidplan_item(); break;
            case "add_raidplan_item": add_raidplan_item(); break;
            case "add_taktik_symbol": add_taktik_symbol(); break;
            case "get_char_info": get_char_info(); break;
            case "save_char_info": save_char_info(); break;
            case "change_prio_mode": change_prio_mode(); break;
            case "delete_prio_item": delete_prio_item(); break;
            case "register_user": register_user(); break;
            case "get_memberliste": get_memberliste(); break;
            case "change_user_status": change_user_status(); break;
            case "addon_auswahl_speichern": addon_auswahl_speichern(); break;
            case "save_own_boss_img": save_own_boss_img(); break;
            case "bild_melden": bild_melden(); break;
            case "get_language": get_language(); break;
            case "select_language": select_language(); break;
            case "recht_uebertragen": recht_uebertragen(); break;
            case "change_password": change_password(); break;
        }
    }
}

function is_ajax() {
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

function get_boss_liste(){
    $return = $_POST;

    $raid_id = $_POST['raid_id'];

    include('db_lib.php');

    $inhalt = $db->get_bossliste($raid_id);

    $return["inhalt"] = $inhalt;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function import_user(){
    $return = $_POST;

    $user_array    = $_POST['user_array'];
    $import_option = $_POST['import_option'];
    $raid          = $_POST['raid'];

    include('db_lib.php');

    if($import_option == 1){
        $db->clear_user_aktuell($raid);
    }

    foreach ($user_array as $value) {
        $db->set_char_aktuell($value);
    }

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function add_area(){
    $return = $_POST;

    $boss_id = $_POST['boss_id'];

    include('db_lib.php');

    $area_id = $db->add_area(array($boss_id,'BOX'));

    $return["ok"] = 1;
    $return["area_id"] = $area_id;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function save_area_position(){
    $return = $_POST;

    $id   = $_POST['id'];
    $top  = $_POST['a_top'];
    $left = $_POST['a_left'];

    include('db_lib.php');

    $db->save_area_position(array($id,$top,$left));

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function load_area(){
    $return = $_POST;

    $boss_id = $_POST['boss_id'];

    include('db_lib.php');

    list($area_string,$bild,$tank_inhalt,$heal_inhalt,$damage_inhalt) = $db->load_area($boss_id);

    $return["ok"] = 1;
    $return["area_string"] = $area_string;
    $return["hintergrundbild"] = $bild;
    $return["tank_inhalt"] = $tank_inhalt;
    $return["heal_inhalt"] = $heal_inhalt;
    $return["damage_inhalt"] = $damage_inhalt;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function delete_area(){
    $return = $_POST;

    $area_id = $_POST['area_id'];
    $boss_id = $_POST['boss_id'];

    include('db_lib.php');

    $db->delete_area(array($area_id,$boss_id));

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function edit_text_area(){
    $return = $_POST;

    $area_id = $_POST['area_id'];
    $inhalt  = $_POST['inhalt'];

    include('db_lib.php');

    list($area_inhalt,$area_inhalt_default) = $db->edit_text_area(array($area_id,$inhalt));

    $return["ok"] = 1;
    $return["inhalt"] = $area_inhalt;
    $return["inhalt_default"] = $area_inhalt_default;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function load_userliste(){
    $return = $_POST;

    $boss_id = $_POST['boss_id'];

    include('db_lib.php');

    $userliste       = $db->load_userliste($boss_id);
    $systemuserliste = $db->load_systemuserliste($boss_id);

    $return["ok"] = 1;
    $return["userliste"] = $userliste;
    $return["systemuserliste"] = $systemuserliste;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function drop_user(){
    $return = $_POST;

    $area_id = $_POST['area_id'];
    $user_id = $_POST['user_id'];
    $boss_id = $_POST['boss_id'];

    include('db_lib.php');

    $box_inhalt = $db->drop_user(array($area_id,$user_id,$boss_id));

    $return["ok"] = 1;
    $return["box_inhalt"] = $box_inhalt;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function reset_user(){
    $return = $_POST;

    $user_id = $_POST['user_id'];
    $boss_id = $_POST['boss_id'];

    include('db_lib.php');

    $db->reset_user(array($user_id,$boss_id));

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function clear_userliste(){
    $return = $_POST;

    include('db_lib.php');

    $db->clear_userliste();

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function sort_up(){
    $return = $_POST;

    $boss_id = $_POST['boss_id'];
    $area_id = $_POST['area_id'];
    $user_id = $_POST['user_id'];

    include('db_lib.php');

    $box_inhalt = $db->sort_area(array($boss_id,$area_id,$user_id,'up'));

    $return["ok"] = 1;
    $return["box_inhalt"] = $box_inhalt;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function sort_down(){
    $return = $_POST;

    $boss_id = $_POST['boss_id'];
    $area_id = $_POST['area_id'];
    $user_id = $_POST['user_id'];

    include('db_lib.php');

    $box_inhalt = $db->sort_area(array($boss_id,$area_id,$user_id,'down'));

    $return["ok"] = 1;
    $return["box_inhalt"] = $box_inhalt;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function get_planungsmodus(){
    $return = $_POST;

    $raid_id = $_POST['raid_id'];

    include('db_lib.php');

    $planung = $db->get_planungsmodus($raid_id);

    $return["ok"] = 1;
    $return["planung"] = $planung;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function set_planungsmodus(){
    $return = $_POST;

    $raid_id = $_POST['raid_id'];
    $wert    = $_POST['wert'];

    include('db_lib.php');

    $planung = $db->set_planungsmodus(array($raid_id,$wert));

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function delete_user(){
    $return = $_POST;

    $user_id = $_POST['user_id'];
    $boss_id = $_POST['boss_id'];

    include('db_lib.php');

    $db->delete_user(array($user_id,$boss_id));

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function load_vorlage(){
    $return = $_POST;

    $vorlage_id = '';
    $boss_id    = $_POST['boss_id'];
    if(isset($_POST['vorlage_id'])){
        $vorlage_id = $_POST['vorlage_id'];
    }

    include('db_lib.php');

    $content = $db->load_vorlage(array($boss_id,$vorlage_id));

    $return["content"] = $content;
    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function set_vorlage(){
    $return = $_POST;

    $boss_id    = $_POST['boss_id'];
    $vorlage_id = $_POST['vorlage_id'];

    include('db_lib.php');

    $db->set_vorlage(array($boss_id,$vorlage_id));

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function save_vorlage(){
    $return = $_POST;

    $boss_id    = $_POST['boss_id'];
    $vorlage_name = $_POST['vorlage_name'];

    include('db_lib.php');

    $vorlage_id = $db->save_vorlage(array($boss_id,$vorlage_name));

    $return["vorlage_id"] = $vorlage_id;
    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function delete_vorlage(){
    $return = $_POST;

    $vorlage_id = $_POST['vorlage_id'];

    include('db_lib.php');

    $db->delete_vorlage($vorlage_id);

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function get_class_info(){
    $return = $_POST;

    $boss_id = $_POST['boss_id'];
    $klasse  = $_POST['klasse'];

    include('db_lib.php');

    $ausgabe = $db->get_class_info(array($boss_id,$klasse));

    $return["ausgabe"] = $ausgabe;
    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function save_class_info(){
    $return = $_POST;

    $boss_id = $_POST['boss_id'];
    $klasse  = $_POST['klasse'];
    $inhalt  = $_POST['inhalt'];

    include('db_lib.php');

    $db->save_class_info(array($boss_id,$klasse,$inhalt));

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function gruppe_ausgabe(){
    $return = $_POST;

    $boss_id = $_POST['boss_id'];

    include('db_lib.php');

    $dialog_gruppe = $db->gruppe_ausgabe(array($boss_id));

    $return["dialog_gruppe"] = $dialog_gruppe;
    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function set_user_to_grp(){
    $return = $_POST;

    $boss_id = $_POST['boss_id'];
    $user_id  = $_POST['user_id'];
    $gruppe_id  = $_POST['gruppe_id'];

    include('db_lib.php');

    list($einzelgruppe,$userliste,$gruppe_alt,$gruppe_alt_id) = $db->set_user_to_grp(array($boss_id,$user_id,$gruppe_id));

    $return["gruppe_ausgabe"] = $einzelgruppe;
    $return["gruppe_alt"] = $gruppe_alt;
    $return["gruppe_alt_id"] = $gruppe_alt_id;
    $return["userliste"] = $userliste;
    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function change_gruppe_user(){
    $return = $_POST;

    $boss_id = $_POST['boss_id'];
    $user_move  = $_POST['user_move'];
    $user_stay  = $_POST['user_stay'];

    include('db_lib.php');

    list($user1,$user2,$grp1,$grp2,$userliste) = $db->change_gruppe_user(array($boss_id,$user_move,$user_stay));

    $return["user1"] = $user1;
    $return["user2"] = $user2;
    $return["grp1"] = $grp1;
    $return["grp2"] = $grp2;
    $return["userliste"] = $userliste;
    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function return_user_gruppe(){
    $return = $_POST;

    $boss_id = $_POST['boss_id'];
    $user_id = $_POST['user_id'];

    include('db_lib.php');

    list($userliste,$gruppe,$gruppe_id) = $db->return_user_gruppe(array($boss_id,$user_id));

    $return["userliste"] = $userliste;
    $return["gruppe"] = $gruppe;
    $return["gruppe_id"] = $gruppe_id;
    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function set_user_via_boss_id(){
    $return = $_POST;

    $boss_id = $_POST['boss_id'];
    $boss_id_alt = $_POST['boss_id_alt'];

    include('db_lib.php');

    $db->set_user_via_boss_id(array($boss_id,$boss_id_alt));

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function reset_gruppen_user(){
    $return = $_POST;

    $boss_id = $_POST['boss_id'];

    include('db_lib.php');

    $db->reset_gruppen_user($boss_id);

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function get_raidplan(){
    $return = $_POST;

    include('db_lib.php');

    $ausgabe = $db->get_raidplan();

    $return["ausgabe"] = $ausgabe;
    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function delete_raidplan_item(){
    $return = $_POST;

    $planer_id = $_POST['planer_id'];

    include('db_lib.php');

    $db->delete_raidplan_item($planer_id);

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function add_raidplan_item(){
    $return = $_POST;

    $planer_id = $_POST['planer_id'];

    include('db_lib.php');

    $db->add_raidplan_item($planer_id);

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function add_taktik_symbol(){
    $boss_id = $_POST['boss_id'];

    include('db_lib.php');

    $area_id = $db->add_area(array($boss_id,'TAKTIK'));

    $return2["ok"] = 1;

    $return["json"] = json_encode($return2);
    echo json_encode($return);
}

function get_char_info(){
    $char_id = $_POST['char_id'];

    include('db_lib.php');

    $inhalt = $db->get_char_info($char_id);

    $return["inhalt"] = $inhalt;
    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function save_char_info(){
    $char_id = $_POST['char_id'];
    $klasse  = $_POST['klasse'];

    include('db_lib.php');

    $db->save_char_info(array($char_id,$klasse));

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function change_prio_mode(){
    $prio_id = $_POST['prio_id'];

    include('db_lib.php');

    $button_text = $db->change_prio_mode($prio_id);

    $return["button_text"] = $button_text;
    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function delete_prio_item(){
    $item_id = $_POST['item_id'];

    include('db_lib.php');

    $check = $db->delete_prio_item($item_id);

    $return["ok"] = 1;
    $return["check"] = $check;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function register_user(){
    $username  = $_POST['username'];
    $passwort1 = $_POST['passwort1'];
    $passwort2 = $_POST['passwort2'];
    $email     = $_POST['email'];
    $realm     = $_POST['realm'];
    $fraktion  = $_POST['fraktion'];
    $gilde     = $_POST['gilde'];

    include('db_lib.php');

    list($ausgabe,$erfolgreich) = $db->register_user(array($username,$passwort1,$passwort2,$email,$realm,$fraktion,$gilde));

    $return["ok"] = 1;
    $return["ausgabe"] = $ausgabe;
    $return["erfolgreich"] = $erfolgreich;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function get_memberliste(){
    include('db_lib.php');

    $ausgabe = $db->get_memberliste();

    $return["ok"] = 1;
    $return["ausgabe"] = $ausgabe;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function change_user_status(){
    $user_id  = $_POST['user_id'];

    include('db_lib.php');

    $ausgabe = $db->change_user_status($user_id);

    $return["ok"] = 1;
    $return["ausgabe"] = $ausgabe;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function addon_auswahl_speichern(){
    $addon  = $_POST['addon'];

    include('db_lib.php');

    $db->addon_auswahl_speichern($addon);

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function save_own_boss_img(){
    $boss_img = $_POST['boss_img'];
    $boss_id  = $_POST['boss_id'];

    include('db_lib.php');

    $mailtext = $db->save_own_boss_img(array($boss_img,$boss_id));

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function bild_melden(){
    $boss_id = $_POST['boss_id'];
    $token  = $_POST['token'];

    include('db_lib.php');

    $db->bild_melden(array($boss_id,$token));

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function get_language(){
    include('db_lib.php');

    $inhalt = $db->get_language();

    $return["ok"] = 1;
    $return["inhalt"] = $inhalt;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function select_language(){
    $sprache = $_POST['sprache'];

    include('db_lib.php');

    $db->select_language($sprache);

    $return["ok"] = 1;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function recht_uebertragen(){
    $target_id = $_POST['target_id'];

    include('db_lib.php');

    $fehler = $db->recht_uebertragen($target_id);

    $return["ok"] = 1;
    $return["fehler"] = $fehler;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

function change_password(){
    $pw1 = $_POST['pw1'];
    $pw2 = $_POST['pw2'];
    $pw3 = $_POST['pw3'];

    include('db_lib.php');

    $fehler = $db->change_password(array($pw1,$pw2,$pw3));

    $return["ok"] = 1;
    $return["fehler"] = $fehler;

    $return["json"] = json_encode($return);
    echo json_encode($return);
}

?>
