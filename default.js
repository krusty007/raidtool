function log(inhalt){
    console.log(inhalt);
    return;
}

$( document ).ready(function() {
    set_hash();
    if($("#hidden_logout").length){
        $("#userliste, #systemuserliste").droppable({tolerance: "pointer", drop: function( event, ui ) {
            var box_id = ui.draggable.attr("id");
            if(box_id.match(/box_item/g)){
                reset_element(box_id);
            }
        }});

        $("#delete_area").droppable({tolerance: "pointer", drop: function( event, ui ) {
            var box_id = ui.draggable.attr("id");
            delete_element(box_id);
        }});
    }

    init_dragable();

    load_addon_liste_via_boss();

    $("#token_input1").focus().addClass("token_bg");
});

function init_dragable() {
    $("#dialogbox_gruppe").draggable({ scroll: true, scrollSensitivity: 100, containment: "document", handle: "p[id^=dragable]" });
    $("#dialogbox_info").draggable({ scroll: true, scrollSensitivity: 100, containment: "document", handle: "p[id^=dragable]" });
    $("div[id^=gruppe_item_]").draggable({ cursor: "move", cursorAt: { top: 9, left: 1 } ,opacity: 0.7, helper: "clone", containment: 'window', scroll: false });
    $(".gruppe_element_user").draggable({ cursor: "move", cursorAt: { top: 9, left: 1 } ,opacity: 0.7, helper: "clone", containment: 'window', scroll: false });

    $("div[id^=grp_elem_]").droppable({tolerance: "pointer", drop: function( event, ui ) {
        var box_id = ui.draggable.attr("id");
        var user_id = $(this).attr("id");
        set_user_gruppe(box_id,user_id);
    }});

    $(".gruppe_element_user").droppable({tolerance: "pointer", drop: function( event, ui ) {
        var box_id = ui.draggable.attr("id");
        var user_id = $(this).attr("id");
        change_gruppe_user(box_id,user_id);
    }});

    $(".gruppe_userliste").droppable({tolerance: "pointer", drop: function( event, ui ) {
        var box_id = ui.draggable.attr("id");
        return_user_gruppe(box_id);
    }});
}

function load_addon_liste(id) {
    if(!id){
        id = $("#default_addon").val();
    }

    $("#schlachtzug option").hide();
    $("#schlachtzug option:eq(0)").show();
    $("#schlachtzug option[data-addon="+id+"]").show();

    $("#boss_liste").html("");

    $(".addon_button").css("background-color","");
    $("#addon_button_"+id).css("background-color","#AEDEE7");
}

function load_addon_liste_via_boss() {
    var addon = $("#schlachtzug option:selected").data("addon");
    load_addon_liste(addon);
}

function load_boss_liste(raid_id,boss_id,raidplan) {
    if(!raid_id){
        raid_id = $("#schlachtzug option:selected").val();
    }else{
        $("#schlachtzug").val(raid_id);
    }

    window.location.hash = '';

    if(!boss_id){
        set_hash();
    }

    $("#main, #userliste, #add_area, #div_import, #clear_userliste, #btn_refersh, #fenster_statistik, #fenster_planung, #systemuserliste, #delete_area, #vorlage_div, #fenster_gruppenplanung, .info_div, .taktik_main, .meldung_div").hide();

    close_info();
    close_gruppe(1);
    $("#prev_boss, #next_boss").hide();

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'get_boss_liste',
            raid_id : raid_id
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        $('#boss_liste').html(json.inhalt);

        if(raid_id){
            $("#fenster_planung").show();
        }

        if(boss_id){
            select_raidaufstellung(boss_id);
        }

        if(raidplan === 1){
            open_raidplan();
        }
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

var boss_id;
function select_raidaufstellung(boss_id_load) {
    var raid_id = $("#schlachtzug option:selected").val();

    if(boss_id_load){
        $("#boss_liste").val(boss_id_load);
    }

    boss_id = $("#boss_liste").val();

    window.document.title = "Raideinteilung";

    close_info();

    if(boss_id){
        $("#main, #userliste, #add_area, #div_import, #clear_userliste, #btn_refersh, #fenster_statistik, #systemuserliste, #delete_area, #vorlage_div, #fenster_gruppenplanung, .info_div, .taktik_main").show();
        load_userliste();
        load_area(1);
        load_vorlage();
        close_gruppe(1);

        set_hash(raid_id,boss_id);

        window.document.title = $("#boss_liste option:selected").text() + " - Raideinteilung";
    }
    else{
        $("#main, #userliste, #add_area, #div_import, #clear_userliste, #btn_refersh, #fenster_statistik, #systemuserliste, #delete_area, #vorlage_div, #fenster_gruppenplanung, .info_div, .taktik_main, .meldung_div").hide();
    }

    set_boss_control_buttons();
}

function user_import() {
    var inhalt = $("#import_text").val();

    if(inhalt.trim()){
        var user_array = inhalt.split(",");
        var import_option = $("input[id^=import_option_]:checked").attr("id").split('_')[2];
        var raid = $("#schlachtzug").val();

        var request = $.ajax({
            url: "ajax.php",
            data: {
                action  : 'import_user',
                user_array : user_array,
                raid : raid,
                import_option : import_option
            },
            method: "POST",
            dataType: "json"
        });

        request.done(function( json ) {
            $("#import_text").val("");
            load_userliste();
            if(import_option === '1'){
                load_area();
            }
        });

        request.fail(function( jqXHR, textStatus ) {
            log( textStatus );
            log( jqXHR );
        });
    }
}

function add_yard() {
    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'add_area',
            boss_id : boss_id
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        load_area();

        set_last_change();
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function save_position(element){
    var left    = $(element).position().left;
    var top     = $(element).position().top;
    var id      = $(element).attr("id").split('_')[1];

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action : 'save_area_position',
            a_top  : top,
            a_left : left,
            id     : id
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        set_last_change();
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function load_area(set_bg) {
    var token = get_url_param("token");
    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'load_area',
            boss_id : boss_id,
            token   : token
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        if(set_bg === 1){
            $("#main").css("background-image", "");

            if(json.hintergrundbild){
                var bg_bild = json.hintergrundbild;

                var bild_url = 'boss_maps/';
                $(".meldung_div").hide();
                $("#boss_bild_url").val("");
                if(/(\/|\\)+/.test(bg_bild)){
                    bild_url = '';
                    if($("#boss_bild_url").length){
                        $("#boss_bild_url").val(json.hintergrundbild);
                        $(".meldung_div").show();
                    }
                }

                bild_url += json.hintergrundbild;

                $("#main").css("background-image", "url("+bild_url+")");
            }
        }

        $("#main").find("div[id^=area_]").remove();
        $("#main").find("img[id^=area_]").remove();

        if(json.area_string){
            $("#main").append(json.area_string);

            if($("#hidden_logout").length){
                $(".area").draggable({ containment: "#main", scroll: false, handle: ".area_title",stop: function() {save_position(this);}});
                $(".taktik_area").draggable({ containment: "#main", scroll: false, stop: function() {save_position(this);}});
                $(".area").droppable({tolerance: "pointer", drop: function( event, ui ) {
                    var area_id = $(this).attr("id");
                    var box_id = ui.draggable.attr("id");
                    drop_element(area_id,box_id);
                }});
                $("div[id^=box_item_]").draggable({ cursor: "move", cursorAt: { top: 9, left: 1 } ,opacity: 0.7, helper: "clone", appendTo: 'body', containment: 'window', scroll: false });
            }
        }

        $(".info_tank").addClass("img_filter_grau");
        $(".info_heal").addClass("img_filter_grau");
        $(".info_damage").addClass("img_filter_grau");

        if(json.tank_inhalt){$(".info_tank").removeClass("img_filter_grau");}
        if(json.heal_inhalt){$(".info_heal").removeClass("img_filter_grau");}
        if(json.damage_inhalt){$(".info_damage").removeClass("img_filter_grau");}

        refresh_statistik();
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function delete_area(area_id,force) {
    var area_text = $("#area_"+area_id).find("div.area_title").text().trim();
    if(!area_text){
        area_text = $("#area_"+area_id).find("input").val();
    }

    var set_delete = 0;
    if(!force){
        if(confirm('Den Bereich "'+area_text+'" wirklich löschen?')){
            set_delete = 1;
        }
    }
    else if(force){
        set_delete = 1;
    }

    if(set_delete === 1){
        var request = $.ajax({
            url: "ajax.php",
            data: {
                action  : 'delete_area',
                area_id : area_id,
                boss_id : boss_id
            },
            method: "POST",
            dataType: "json"
        });

        request.done(function( json ) {
            $("#area_"+area_id).remove();

            load_userliste();

            set_last_change();
        });

        request.fail(function( jqXHR, textStatus ) {
            log( textStatus );
            log( jqXHR );
        });
    }
}

function edit_area(area_id) {
    var inhalt = $("#area_"+area_id).find("div.area_title").data("default");
    $("#area_"+area_id).find("span").html('<input type="text" value="'+inhalt+'" id="area_edit_'+area_id+'" style="width:90px;" onkeyup="enter_area_text(event,this);">');
    $("#area_edit_"+area_id).select().focus().after(' <img src="disable.png" style="height:16px;cursor:pointer;" id="btn_cancel_'+area_id+'" onclick="cancel_edit('+area_id+');">');
    $("#btn_edit_"+area_id).remove();
    set_last_change();
}

function cancel_edit(area_id) {
    var inhalt = $("#area_"+area_id).find("div.area_title").data("default");
    inhalt = replace_symbole(inhalt);
    $("#area_"+area_id).find("span").html(inhalt).after(' <img src="edit.png" id="btn_edit_'+area_id+'" style="cursor:pointer;" onclick="edit_area(\''+area_id+'\');">');
    $("#btn_cancel_"+area_id).remove();
}

function replace_symbole(inhalt) {
    var inhalt_neu = inhalt;

    inhalt_neu = inhalt_neu.replace(/{TOTENKOPF}/i,'<img src="symbole/totenkopf.png">');
    inhalt_neu = inhalt_neu.replace(/{KREUZ}/i,'<img src="symbole/kreuz.png">');
    inhalt_neu = inhalt_neu.replace(/{QUADRAT}/i,'<img src="symbole/quadrat.png">');
    inhalt_neu = inhalt_neu.replace(/{MOND}/i,'<img src="symbole/mond.png">');
    inhalt_neu = inhalt_neu.replace(/{DREIECK}/i,'<img src="symbole/dreieck.png">');
    inhalt_neu = inhalt_neu.replace(/{DIAMANT}/i,'<img src="symbole/diamant.png">');
    inhalt_neu = inhalt_neu.replace(/{KREIS}/i,'<img src="symbole/kreis.png">');
    inhalt_neu = inhalt_neu.replace(/{STERN}/i,'<img src="symbole/stern.png">');
    inhalt_neu = inhalt_neu.replace(/&&/i,'<br>');

    return inhalt_neu;
}

function enter_area_text(event,element) {
    if(event.keyCode === 13){
        var inhalt = $(element).val();
        var area_id = $(element).attr("id").split('_')[2];

        var request = $.ajax({
            url: "ajax.php",
            data: {
                action  : 'edit_text_area',
                area_id : area_id,
                inhalt  : inhalt
            },
            method: "POST",
            dataType: "json"
        });

        request.done(function( json ) {
            cancel_edit(area_id);
            $("#area_"+area_id).find("div.area_title").data("default",json.inhalt_default);
            $("#area_"+area_id).find("span").html(json.inhalt);
            set_last_change();
        });

        request.fail(function( jqXHR, textStatus ) {
            log( textStatus );
            log( jqXHR );
        });
    }
    else if(event.keyCode === 27){
        var area_id = $(element).attr("id").split('_')[2];
        cancel_edit(area_id);
    }
}

function load_userliste() {
    var token = get_url_param("token");

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'load_userliste',
            boss_id : boss_id,
            token : token
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        $("#userliste").html(json.userliste);
        $("#systemuserliste").html(json.systemuserliste);

        if($("#hidden_logout").length){
            $("div[id^=box_item_]").draggable({ cursor: "move", cursorAt: { top: 9, left: 1 } ,opacity: 0.7, helper: "clone", appendTo: 'body', containment: 'window', scroll: false });
        }
        refresh_statistik();
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function set_hash(raid_id,boss_id) {
    if(!window.location.hash){
        window.location.hash = '';
    }

    if(boss_id){
        var url_hash = 'raid='+raid_id+'&boss='+boss_id;
        window.location.hash = url_hash;
    }else{
        if(window.location.hash.split(/#/)[1]){
            goto_boss();
        }
    }
}

function goto_boss() {
    var hash = window.location.hash;
    var inhalt = hash.split(/#/)[1];
    var params = inhalt.split(/&/);

    var raid_id;
    var boss_id;
    params.forEach(function(element){
        var parameter = element.split(/=/)[0];
        var wert = element.split(/=/)[1];

        if(parameter === 'raid'){
            raid_id = wert;
        }
        else if(parameter === 'boss'){
            boss_id = wert;
        }
    });

    load_boss_liste(raid_id,boss_id);
}

function drop_element(drop_id,box_id) {
    if(box_id.split('_')[0] != 'box'){
        return;
    }

    var area_id = drop_id.split('_')[1];
    var user_id = box_id.split('_')[2];

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'drop_user',
            user_id : user_id,
            boss_id : boss_id,
            area_id : area_id
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        $("#"+box_id).remove();
        $("#"+drop_id).find("div[id^=box_item_]").remove();
        $("#"+drop_id).append(json.box_inhalt);

        if($("#hidden_logout").length){
            $("div[id^=box_item_]").draggable({ cursor: "move", cursorAt: { top: 9, left: 1 } ,opacity: 0.7, helper: "clone", appendTo: 'body', containment: 'window', scroll: false });
        }
        refresh_statistik();
        set_last_change();
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function reset_element(box_id) {
    var user_id = box_id.split('_')[2];

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'reset_user',
            user_id : user_id,
            boss_id : boss_id
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        $("#"+box_id).remove();

        load_userliste();
        load_area();
        set_last_change();
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function clear_userliste() {
    if(confirm("Alle aktuellen Charaktere entfernen?")){
        var request = $.ajax({
            url: "ajax.php",
            data: {
                action  : 'clear_userliste'
            },
            method: "POST",
            dataType: "json"
        });

        request.done(function( json ) {
            load_userliste();
            load_area();
        });

        request.fail(function( jqXHR, textStatus ) {
            log( textStatus );
            log( jqXHR );
        });
    }
}

function sort_up(user_id) {
    var token = get_url_param("token");
    var area_id = $("#box_item_"+user_id).closest("div[id^=area_]").attr("id").split('_')[1];

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'sort_up',
            boss_id : boss_id,
            area_id : area_id,
            user_id : user_id,
            token : token
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        $("#area_"+area_id).find("div[id^=box_item_]").remove();
        $("#area_"+area_id).append(json.box_inhalt);

        if($("#hidden_logout").length){
            $("div[id^=box_item_]").draggable({ cursor: "move", cursorAt: { top: 9, left: 1 } ,opacity: 0.7, helper: "clone", appendTo: 'body', containment: 'window', scroll: false });
        }
        set_last_change();
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function sort_down(user_id) {
    var token = get_url_param("token");
    var area_id = $("#box_item_"+user_id).closest("div[id^=area_]").attr("id").split('_')[1];

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'sort_down',
            boss_id : boss_id,
            area_id : area_id,
            user_id : user_id,
            token : token
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        $("#area_"+area_id).find("div[id^=box_item_]").remove();
        $("#area_"+area_id).append(json.box_inhalt);

        if($("#hidden_logout").length){
            $("div[id^=box_item_]").draggable({ cursor: "move", cursorAt: { top: 9, left: 1 } ,opacity: 0.7, helper: "clone", appendTo: 'body', containment: 'window', scroll: false });
        }
        set_last_change();
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function refresh_statistik() {
    var char_liste = $("#userliste").find("div[id^=box_item_]").length;
    $("#anzahl_liste").html(char_liste);

    var area_char_liste = $("#main").find("div[id^=box_item_][data-system=0]").length;
    $("#anzahl_bereiche").html(area_char_liste);

    var area_systemchar_liste = $("#main").find("div[id^=box_item_]").length;

    var gesamt = parseInt(char_liste) + parseInt(area_char_liste);
    $("#anzahl_gesamt").html(gesamt);
}

function get_planungsmodus() {
    var raid_id = $("#schlachtzug option:selected").val();

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'get_planungsmodus',
            raid_id : raid_id
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        if(json.planung === "1"){
            $("#planung_status").html("EIN");
            $("#planung_button").text("Ausschalten").attr("onclick","set_planung('aus');");
        }else{
            $("#planung_status").html("AUS");
            $("#planung_button").text("Einschalten").attr("onclick","set_planung('an');");;
        }
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function set_planung(wert) {
    var raid_id = $("#schlachtzug option:selected").val();

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'set_planungsmodus',
            raid_id : raid_id,
            wert : wert
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        get_planungsmodus();
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function get_url_param(param){
    "use strict";

    var uri_aktuell = window.location.href;
    var regex = new RegExp("[\\?&]" + param + "=(.*?)(#|&|$)");
    if (uri_aktuell.match(regex)){
        return uri_aktuell.match(regex)[1];
    }
    else{
        return "";
    }
}

function remove_url_param(url,param) {
    if(!url){
        url = window.location.href;
    }

    if(param){
        var regex = new RegExp("[\\?&]" + param + "=(.*?)(#|&|$)");
        url = url.replace(regex, "$2");
    }

    return url;
}

function delete_element(box_id) {
    var user_id = box_id.split('_')[2];

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'delete_user',
            user_id : user_id,
            boss_id : boss_id
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        $("#"+box_id).remove();

        load_userliste();
        load_area();
        set_last_change();
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function load_vorlage(set_id) {
    var vorlage_id = $("#vorlage_vorhanden").val();

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'load_vorlage',
            vorlage_id : vorlage_id,
            boss_id : boss_id
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        $("#vorlage_vorhanden").html(json.content);
        if(set_id){
            $("#vorlage_vorhanden").val(set_id);
        }
        $("#vorlage_delete").html(json.content);
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function set_vorlage() {
    var vorlage_id = $("#vorlage_vorhanden").val();

    if(vorlage_id){
        var request = $.ajax({
            url: "ajax.php",
            data: {
                action  : 'set_vorlage',
                vorlage_id : vorlage_id,
                boss_id : boss_id
            },
            method: "POST",
            dataType: "json"
        });

        request.done(function( json ) {
            load_userliste();
            load_area();
            set_last_change();
        });

        request.fail(function( jqXHR, textStatus ) {
            log( textStatus );
            log( jqXHR );
        });
    }
}

function save_vorlage() {
    var vorlage_name = $("#vorlage_name").val();

    if(vorlage_name){
        var request = $.ajax({
            url: "ajax.php",
            data: {
                action  : 'save_vorlage',
                vorlage_name : vorlage_name,
                boss_id : boss_id
            },
            method: "POST",
            dataType: "json"
        });

        request.done(function( json ) {
            $("#vorlage_name").val("")
            load_vorlage(json.vorlage_id);
        });

        request.fail(function( jqXHR, textStatus ) {
            log( textStatus );
            log( jqXHR );
        });
    }
}

function delete_vorlage() {
    var vorlage_name = $("#vorlage_delete option:selected").text();

    if(confirm('Soll die Vorlage "'+vorlage_name+'" wirklich gelöscht werden?')){
        var vorlage_id = $("#vorlage_delete").val();
        var set_id     = $("#vorlage_vorhanden").val();

        if(vorlage_name){
            var request = $.ajax({
                url: "ajax.php",
                data: {
                    action  : 'delete_vorlage',
                    vorlage_id : vorlage_id
                },
                method: "POST",
                dataType: "json"
            });

            request.done(function( json ) {
                if(vorlage_id === set_id){
                    set_id = "";
                }
                load_vorlage(set_id);
            });

            request.fail(function( jqXHR, textStatus ) {
                log( textStatus );
                log( jqXHR );
            });
        }
    }
}

function open_gruppe(reload) {
    if($("#dialogbox_gruppe").is(":visible")){
        close_gruppe();
        if(reload){
            open_gruppe(1);
        }
    }else{
        $("#gruppe_button").text("Schließen");

        if($("#dialogbox_gruppe").length && !reload){
            $("#dialogbox_gruppe").show();
        }
        else{
            var request = $.ajax({
                url: "ajax.php",
                data: {
                    action  : 'gruppe_ausgabe',
                    boss_id : boss_id
                },
                method: "POST",
                dataType: "json"
            });

            request.done(function( json ) {
                if(json.dialog_gruppe){
                    $("#dialogbox_gruppe").remove();
                    $("body").prepend(json.dialog_gruppe);

                    init_dragable();
                }
            });

            request.fail(function( jqXHR, textStatus ) {
                log( textStatus );
                log( jqXHR );
            });
        }
    }
}

function close_gruppe(remove) {
    if(remove){
        $("#dialogbox_gruppe").remove();
    }
    else{
        $("#dialogbox_gruppe").hide();
    }
    $("#gruppe_button").text("Anzeigen");
}

function set_user_gruppe(user_id,gruppe_id) {
    if(user_id.split(/_/)[0] != 'gruppe'){
        return;
    }

    user_id = user_id.split(/_/)[2];
    gruppe_id = gruppe_id.split(/_/)[2];

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'set_user_to_grp',
            boss_id : boss_id,
            user_id : user_id,
            gruppe_id : gruppe_id
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        if(json.gruppe_ausgabe){
            $("#gruppe_item_"+user_id).remove();
            $("#gruppe_"+gruppe_id).replaceWith(json.gruppe_ausgabe);
            $("#gruppe_"+json.gruppe_alt_id).replaceWith(json.gruppe_alt);
            $(".gruppe_userliste").html(json.userliste);

            init_dragable();
        }
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function get_class_info(klasse) {
    var token = get_url_param("token");

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'get_class_info',
            boss_id : boss_id,
            klasse : klasse,
            token : token
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        dialogbox(json.ausgabe);
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function close_info() {
    $("#dialogbox_info").hide();
    $("#token_input1").focus().addClass("token_bg");
}

function save_class_info(klasse) {
    var inhalt = $("#class_inhalt").val();
    var token = get_url_param("token");

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'save_class_info',
            boss_id : boss_id,
            inhalt : inhalt,
            klasse : klasse,
            token : token
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        $("#dialogbox_info").effect( "highlight", {color: '#73E491'}, 1000 );
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function change_gruppe_user(box_id,user_id) {
    var user_move = box_id.split('_')[2];
    var user_stay = user_id.split('_')[2];

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'change_gruppe_user',
            boss_id : boss_id,
            user_move : user_move,
            user_stay : user_stay
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        if(json.user1){
            $("#gruppe_"+json.grp1).replaceWith(json.user1);
        }

        if(json.user2){
            $("#gruppe_"+json.grp2).replaceWith(json.user2);
        }

        if(json.userliste){
            $(".gruppe_userliste").html(json.userliste);
        }

        init_dragable();
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function return_user_gruppe(box_id) {
    if(box_id.split(/_/)[0] != 'gruppe'){
        return;
    }

    var user_id = box_id.split('_')[2];

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'return_user_gruppe',
            boss_id : boss_id,
            user_id : user_id
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        if(json.userliste){
            $("#gruppe_"+json.gruppe_id).replaceWith(json.gruppe);
            $(".gruppe_userliste").html(json.userliste);
        }

        init_dragable();
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function set_user_via_boss_id() {
    var boss_id_alt = $("#gruppe_boss_auswahl").val();
    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'set_user_via_boss_id',
            boss_id : boss_id,
            boss_id_alt : boss_id_alt
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        open_gruppe(1);
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function reset_gruppen_user() {
    if(confirm("Alle Charaktere wirklich zurücklegen?")){
        var request = $.ajax({
            url: "ajax.php",
            data: {
                action  : 'reset_gruppen_user',
                boss_id : boss_id
            },
            method: "POST",
            dataType: "json"
        });

        request.done(function( json ) {
            open_gruppe(1);
        });

        request.fail(function( jqXHR, textStatus ) {
            log( textStatus );
            log( jqXHR );
        });
    }
}

function gruppe_exportieren() {
    if(!$("#textfeld_csv_export").length){
        var url = '<br><br><div style="font-size:14px;margin-left:523px;">Für den Export braucht man das Addon Raid Group Organizer. Dieses kannst Du <a href="https://www.curseforge.com/wow/addons/raid-group-organizer" target="_blank">hier</a> downloaden.</div>';
        $("#button_grp_export").after('<br><textarea id="textfeld_csv_export" rows="11" cols="35" style="margin-left:10px;"></textarea><br><button style="margin-left:10px;" onclick="csv_copy();">Inhalt kopieren</button>'+url);
    }

    var ausgabe = '';
    for (var i = 1; i <= 8; i++) {
        $.each($("#gruppe_"+i).find("div"),function(){
            if($(this).hasClass("gruppe_element_full")){
                ausgabe += $(this).text().trim()+',';
            }
            else if($(this).hasClass("gruppe_element")){
                ausgabe += ' ,';
            }
        });
    }

    $("#textfeld_csv_export").val(ausgabe);
}

function csv_copy() {
    $("#textfeld_csv_export").select();
    document.execCommand("copy");
}

function set_boss_control_buttons() {
    var select_boss = $("#boss_liste").val();
    $("#prev_boss, #next_boss").hide().attr("onclick","");
    $("#prev_boss").attr("src","up.png");
    $("#next_boss").attr("src","down.png");
    if(select_boss){
        if($("#boss_liste")[0].selectedIndex <= 1){
            $("#prev_boss").attr("src","up_hover.png").removeClass("boss_controll");
        }
        else{
            $("#prev_boss").attr("onclick","goto_prev_boss()").addClass("boss_controll");
        }

        if($("#boss_liste")[0].selectedIndex >= $("#boss_liste option").length - 1){
            $("#next_boss").attr("src","down_hover.png").removeClass("boss_controll");
        }
        else{
            $("#next_boss").attr("onclick","goto_next_boss()").addClass("boss_controll");;
        }

        $("#prev_boss, #next_boss").show();
    }
}

function goto_prev_boss() {
    var boss = $("#boss_liste option").eq($("#boss_liste")[0].selectedIndex - 1).val();

    select_raidaufstellung(boss);
}

function goto_next_boss() {
    var boss = $("#boss_liste option").eq($("#boss_liste")[0].selectedIndex + 1).val();

    select_raidaufstellung(boss);
}

function open_raidplan() {
    var request = $.ajax({
        url: "ajax.php",
        data: {
            action : 'get_raidplan'
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        dialogbox(json.ausgabe);

        $.each($("td[id^=raidplan_item_]"),function(){
            if($(this).attr("id").split('_')[2] === boss_id){
                $(this).addClass("raidplaner_marker");
            }
        });
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function delete_raidplan_item(planer_id) {
    var request = $.ajax({
        url: "ajax.php",
        data: {
            action : 'delete_raidplan_item',
            planer_id: planer_id
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        $("#planer_row_"+planer_id).remove();
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function add_raidplan_item() {
    var boss_auswahl_id = $("#edit_raidplan_table").find("select[id=gruppe_boss_auswahl]").val();

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action : 'add_raidplan_item',
            planer_id: boss_auswahl_id
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        open_raidplan();
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function set_last_change() {
    var currentdate = new Date();

    var tag     = ((currentdate.getDate() < 10)?"0":"") + currentdate.getDate();
    var monat   = (((currentdate.getMonth()+1) < 10)?"0":"") + (currentdate.getMonth()+1);
    var jahr    = currentdate.getFullYear();
    var stunde  = ((currentdate.getHours() < 10)?"0":"") + currentdate.getHours();
    var minute  = ((currentdate.getMinutes() < 10)?"0":"") + currentdate.getMinutes();
    var sekunde = ((currentdate.getSeconds() < 10)?"0":"") + currentdate.getSeconds();

    var datum = jahr + '-' + monat + '-' + tag + ' ' + stunde + ':' + minute + ':' + sekunde;

    $("#last_change").val(datum);
}

function toggle_taktik() {
    $(".taktik_box").animate({
        width: "toggle"
    },function(){
        if($(".taktik_box").is(":visible")){
            $(".switcher").text(">");
        }else{
            $(".switcher").text("<");
        }
    });
}

function add_taktik_symbol(symbol_id) {
    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'add_taktik_symbol',
            boss_id : boss_id,
            symbol_id : symbol_id
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        load_area();

        set_last_change();
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function delete_taktik(id) {
    if(confirm('Taktiksymbol wirklich löschen?')){
        delete_area(id,1);
    }
}

function change_addon(objekt) {
    var addon_id = $(objekt).data("addon_id");

    $("#schlachtzug").val("");

    load_boss_liste();

    load_addon_liste(addon_id);
}

function open_char_edit(char_id) {
    var token = get_url_param("token");
    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'get_char_info',
            char_id : char_id,
            token   : token
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        dialogbox(json.inhalt);
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function save_char_info() {
    var char_id = $("#char_info_id").val();
    var klasse  = $("#klassenwahl").val();
    var token = get_url_param("token");

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'save_char_info',
            char_id : char_id,
            klasse  : klasse,
            token   : token
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        $("#dialogbox_info").effect( "highlight", {color: '#73E491'}, 1000 );

        load_userliste();
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function change_prio_mode(prio_id) {
    var request = $.ajax({
        url: "ajax.php",
        data: {
            action  : 'change_prio_mode',
            prio_id : prio_id
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        $("#button_"+prio_id).text(json.button_text);
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function delete_prio_item(item_id) {
    var item_text = $("#item_"+item_id).text();
    var charakter = $("#charakter_"+item_id).text();
    if(confirm("Soll das Item '"+item_text+"' von "+charakter+" wirklich gelöscht werden?")){
        var request = $.ajax({
            url: "ajax.php",
            data: {
                action  : 'delete_prio_item',
                item_id : item_id
            },
            method: "POST",
            dataType: "json"
        });

        request.done(function( json ) {
            if(json.check == "1"){
                $("#charakter_"+item_id).closest("tr").remove();
            }
        });

        request.fail(function( jqXHR, textStatus ) {
            log( textStatus );
            log( jqXHR );
        });
    }
}

var register_inhalt;
function open_register() {
    if(!register_inhalt){
        register_inhalt = $("#register_window").html();
        $("#register_window").remove();
    }

    dialogbox(register_inhalt);
}

function close_register() {
    close_info();
    $("#token_input1").focus().addClass("token_bg");
}

function register_me() {
    var username  = $("#register_username").val();
    var passwort1 = $("#register_pw1").val();
    var passwort2 = $("#register_pw2").val();
    var email     = $("#register_email").val();
    var realm     = $("#register_realm").val();
    var fraktion  = $("#register_fraktion").val();
    var gilde     = $("#register_gilde").val();

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action    : 'register_user',
            username  : username,
            passwort1 : passwort1,
            passwort2 : passwort2,
            email     : email,
            realm     : realm,
            fraktion  : fraktion,
            gilde     : gilde
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        if(json.erfolgreich == 1){
            // ausgabe
            var inhalt = 'Eine eMail mit Aktivierungslink ist auf dem Weg zu Dir';

            dialogbox(inhalt);
        }
        else{
            // dialogbox
            dialogbox(json.ausgabe + "<br><br>" + register_inhalt);

            $("#register_username").val(username);
            $("#register_email").val(email);
            $("#register_realm").val(realm);
            $("#register_fraktion").val(fraktion);
            $("#register_gilde").val(gilde);
        }
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR.responseText );
    });
}

function dialogbox(inhalt) {
    if(!$("#dialogbox_info").length){
        $("body").prepend('<div id="dialogbox_info" class="dialogbox_info"><p id="dragable" class="close_dialog"><span class="x_button" onclick="close_info();">X</span></p><div id="info_inhalt" style="padding:10px;"></div></div>');
    }

    $("#info_inhalt").css("text-align","center").html(inhalt);

    $("#dialogbox_info").show();

    init_dragable();
}

function open_token() {
    var token = "";
    token += $("#token_input1").val();
    token += $("#token_input2").val();
    token += $("#token_input3").val();
    token += $("#token_input4").val();
    token += $("#token_input5").val();

    token = token.toUpperCase();
    if(token.trim()){
        var url = window.location.href;

        if(/(.*)#/.test(url)){
            url = RegExp.$1;
        }

        if(/(.*)\?/.test(url)){
            url = RegExp.$1;
        }

        window.location.href = url + "?token="+token;
    }
}

function enter_token(id,event) {
    if(event.keyCode == 32){
        $("#token_input"+id).val($("#token_input"+id).val().trim());
        return;
    }

    if($("#token_input"+id).val().length > 1){
        var inhalt = $("#token_input"+id).val().substring(0, 1);
        $("#token_input"+id).val(inhalt);
    }

    if(id < 6 && id > 0){
        if($("#token_input"+id).val()){
            if(id<5){
                $("#token_input"+(id+1)).focus().select();
            }
        }
        else{
            if(id > 1){
                $("#token_input"+(id-1)).focus().select();
            }
            else{
                $("#token_input1").focus().select();
            }
        }
    }

    if(
        $("#token_input1").val() &&
        $("#token_input2").val() &&
        $("#token_input3").val() &&
        $("#token_input4").val() &&
        $("#token_input5").val()
    ){
        $("#token_button").prop("disabled",false);
    }
    else{
        $("#token_button").prop("disabled",true);
    }
}

function fokusin_token(objekt) {
    $(objekt).addClass("token_bg");
}

function fokusout_token() {
    $("input[id^=token_input]").removeClass("token_bg");
}

function open_memberwindow() {
    var request = $.ajax({
        url: "ajax.php",
        data: {
            action : 'get_memberliste'
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        dialogbox(json.ausgabe);
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function change_user_status(user_id) {
    var request = $.ajax({
        url: "ajax.php",
        data: {
            action : 'change_user_status',
            user_id: user_id
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        if(json.ausgabe){
            $("#spalte_"+user_id).html(json.ausgabe);
        }
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function addon_auswahl_speichern() {
    var addon = $("#addon_auswahl").val();

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action : 'addon_auswahl_speichern',
            addon: addon
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        if(json.ok){
            $(".x_button").attr("onclick","close_info_reload();");
            $("#addon_auswahl_button").effect( "highlight", {color: '#73E491'}, 1000 );
        }
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function close_info_reload() {
    var is_url = window.location.href;

    url = /(.*)#/.test(is_url) ? RegExp.$1 : window.location.href;

    window.location.href=url;
}

function goto(ziel) {
    var url = "";
    switch(ziel) {
        case "home":
            var is_url = window.location.href;
            url = /(^.*)\?token.*/.test(is_url) ? RegExp.$1 : '';
            break;
    }

    if(url.length){
        window.location.href = url;
    }
}

function save_own_boss_img() {
    var image = $("#boss_bild_url").val();

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action : 'save_own_boss_img',
            boss_img : image,
            boss_id : boss_id
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        if(json.ok){
            load_area(1);
        }
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function open_img_meldung() {
    var inhalt = 'Da es sich bei diesem Bild um ein externes Bild handelt, übernehmen wir keine Haftung.<br><br>';
    inhalt += 'Sollten wir Kenntnis von Rechtsverstößen auf dem verlinkten Bild bekommen, werden wir die Verlinkung sofort aufheben.<br><br>';
    inhalt += '<div style="text-align:center;"><button class="meldung_button" onclick="bild_melden();">Anstößiges Bild hier melden</button></div>';

    dialogbox(inhalt);
}

function bild_melden() {
    if(confirm("Anstößiges Bild melden?")){
        var token = get_url_param("token");
        var request = $.ajax({
            url: "ajax.php",
            data: {
                action : 'bild_melden',
                boss_id : boss_id,
                token : token
            },
            method: "POST",
            dataType: "json"
        });

        request.done(function( json ) {
            var inhalt = 'Verstoß wurde gemeldet und wird so schnell wie möglich überprüft';

            dialogbox(inhalt);
        });

        request.fail(function( jqXHR, textStatus ) {
            log( textStatus );
            log( jqXHR );
        });
    }
}

function open_faq() {
    window.open('faq.php');
}

function toggle_antwort(id) {
    $("#antwort_"+id).slideToggle();
}

function open_language_window() {
    var request = $.ajax({
        url: "ajax.php",
        data: {
            action : 'get_language'
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        dialogbox(json.inhalt);
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function select_language(sprache) {
    var request = $.ajax({
        url: "ajax.php",
        data: {
            action : 'select_language',
            sprache: sprache
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        location.reload();
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}

function recht_uebertragen(target_id) {
    if(confirm("Soll das Gildengründungsrecht übertragen werden?")){
        var request = $.ajax({
            url: "ajax.php",
            data: {
                action : 'recht_uebertragen',
                target_id: target_id
            },
            method: "POST",
            dataType: "json"
        });

        request.done(function( json ) {
            if(json.fehler){
                alert(json.fehler);
            }
            else{
                location.reload();
            }
        });

        request.fail(function( jqXHR, textStatus ) {
            log( textStatus );
            log( jqXHR );
        });
    }
}

function open_password_window() {
    var inhalt = '';
    inhalt += '<div id="fehler_info"></div>';
    inhalt += '<table style="margin-left:auto;margin-right:auto;">';
    inhalt += '<tr><td>Passwort (aktuell)</td></tr><tr><td><input type="password" id="pw1" /></td></tr>';
    inhalt += '<tr><td>&nbsp;</td></tr>';
    inhalt += '<tr><td>Passwort (neu)</td></tr><tr><td><input type="password" id="pw2" /></td></tr>';
    inhalt += '<tr><td>&nbsp;</td></tr>';
    inhalt += '<tr><td>Passwort (neu wiederholen)</td></tr><tr><td><input type="password" id="pw3" /></td></tr>';
    inhalt += '<tr><td>&nbsp;</td></tr>';
    inhalt += '<tr><td><button style="margin:5px;" onclick="change_password();">&Auml;ndern</button><button style="margin:5px;" onclick="close_info();">Abbrechen</button></td></tr>';
    inhalt += '</table>';

    dialogbox(inhalt);
}

function change_password() {
    var pw1 = $("#pw1").val();
    var pw2 = $("#pw2").val();
    var pw3 = $("#pw3").val();

    var request = $.ajax({
        url: "ajax.php",
        data: {
            action : 'change_password',
            pw1: pw1,
            pw2: pw2,
            pw3: pw3
        },
        method: "POST",
        dataType: "json"
    });

    request.done(function( json ) {
        if(json.fehler){
            $("#fehler_info").html(json.fehler);
        }
        else{
            alert("Passwort geändert");
            close_info();
        }
    });

    request.fail(function( jqXHR, textStatus ) {
        log( textStatus );
        log( jqXHR );
    });
}
